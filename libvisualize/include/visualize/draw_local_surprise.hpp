#ifndef ROST_DRAW_LOCAL_SURPRISE
#define ROST_DRAW_LOCAL_SURPRISE

#include <algorithm>
#include <numeric>
#include <opencv2/core.hpp>

using namespace std;

template <typename Tp>
cv::Mat draw_pose_surprise(const std::map<std::array<int, 3>, Tp> &pose_surprise, cv::Mat img, bool equalize = true) {
  Tp offset = 0, scale = 1;

  if (equalize) {
    Tp max_surprise = pose_surprise.begin()->second;
    Tp min_surprise = pose_surprise.begin()->second;

    for (auto ps : pose_surprise) {
      max_surprise = max(max_surprise, ps.second);
      min_surprise = min(min_surprise, ps.second);
    }

    offset = min_surprise;
    scale = max_surprise - min_surprise;
  }

  for (auto ps : pose_surprise) {
    auto pose = ps.first;
    img.at<Tp>(pose[1], pose[2]) = (ps.second - offset) / scale;
  }
  return img;
}

#endif
