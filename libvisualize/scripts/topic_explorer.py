#!/usr/bin/env python
import sys, math, os
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4']='PySide'
import pylab
 
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import csv
import matplotlib.image as mpimg
from matplotlib.widgets import Slider, Button, RadioButtons

from PySide import QtCore, QtGui
import linecache
import glob

topics_dir= os.path.abspath(sys.argv[1])
topic_hist_filename=topics_dir+'/topics.hist.csv'
ppx_filename=topics_dir+'/perplexity.csv'
topic_filename=topics_dir+'/topics.maxlikelihood.csv'
topic_position_filename=topics_dir+'/topics.position.csv'
topic_plot_filename=topics_dir+'/topics.plot.png'
word_filename='words/words.all.csv'
word_position_filename='words/words.all.position.csv'

image_glob='images/*.jp*g'
#image_glob='images/*.JPG'
image_filenames=[]
time_viewport=[0,1]
topics=[]
timestamps=[]
topic_order=[]
vocabsize=0
show_words=False
topic_ppx=[]
imgdata=None

def initialize_filename():
    global topic_position_filename
    topic_position_dir=topics_dir
    while not os.path.exists(topic_position_dir+'/topics.position.csv'):
        print "Looked for topics.position file in "+topic_position_dir+'/topics.position.csv'
        if topic_position_dir == '' or topic_position_dir =='/':
            print "Could not find topics.position.csv file"
            sys.exit(0)
        else:
            topic_position_dir=os.path.dirname(topic_position_dir)
    print "Found topics.position.csv in "+topic_position_dir
    topic_position_filename=topic_position_dir+'/topics.position.csv'

def load_topic_data():
    global topics, timestamps, topic_order, vocabsize, topic_ppx

    data=[]
    numcols=0
    with open(topic_hist_filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            numcols = max(numcols, len(row))
            data.append(map(float,row))
    for row in data:
        while len(row) < numcols:
            row.append(0)

    vocabsize=numcols-1

    #data.pop() #remove the last row because for some reason rost is not processing it
    data=np.array(data)
    timestamps=data[:,0]
    topics=data[:,1:]
    topic_weights=np.ones(vocabsize)
    for t in topics:
        topic_weights+=t
        t/=np.sum(t)

    #topic_weights=map(np.sum, np.transpose(data[:,1:]))
    W=np.sum(topic_weights)
    topic_weights/=W
    print 'topic weights:', topic_weights, 'W=',W

    topic_ppx=np.zeros(len(topics))
    minus_log_weights=-np.log(topic_weights)
    print 'topic entropy:', minus_log_weights
    for i in range(len(topics)):
        topic_ppx[i]=np.sum(topics[i] * minus_log_weights)

    #topic_order=np.argsort(map(np.sum, np.transpose(topics)))[::-1]
    topic_order=np.array(range(vocabsize))

def load_image_filenames():
    global image_filenames, image_glob, topics_dir
    image_dir=topics_dir
    image_filenames=[]
    print 'globbing images'
    image_filenames=glob.glob(image_dir+'/'+image_glob)
    while len(image_filenames)==0:
        if image_dir == '' or image_dir =='/':
            print "Could not find images"
            sys.exit(0)        
        image_dir=os.path.dirname(image_dir)
        image_filenames=glob.glob(image_dir+'/'+image_glob)
    print 'Found '+str(len(image_filenames))+' images in '+image_dir+'/'+image_glob
    image_filenames.sort()
    print ''


ppxdata=[]
def load_ppx_data():
    global ppxdata
    data=[]
    with open(ppx_filename, 'rb') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            data.append(map(float,row))
    ppxdata=np.array(data)
    ppxdata=ppxdata[:,1]


def topic_similarity_for_time(t):
    global topics
    return map(lambda x: np.dot(topics[t], x), topics)


def draw_current_time(t):
    global fig,imgplot,topic_filename, topic_position_filename, topic_order, color, x_img, x_imgtopics, x_time, canvas,vocabsize, image_filenames
    global n, time_viewport, max_time, imgdata
    print("Time:",t, "filename: ",image_filenames[t])


    imgdata=mpimg.imread(image_filenames[t])
    imgplot=x_img.imshow(imgdata)  
    x_img.set_title(image_filenames[t], fontsize=60)      
    x_img.set_axis_off()

    #imgplot.set_data(imgdata)
    if show_words:
        word_list=get_data_for_time(word_filename,t)
        word_pos_list=get_data_for_time(word_position_filename,t)
    else:
        word_list=get_data_for_time(topic_filename,t)
        word_pos_list=get_data_for_time(topic_position_filename,t)
    assert len(word_list)*2 == len(word_pos_list),'Invalid data #words:'+str(len(word_list))+'#pos:'+str(len(word_pos_list)) 


    imgtopicplot=x_imgtopics.imshow(render_words2(word_list, word_pos_list, topic_order, imgdata.shape[1], imgdata.shape[0],0.1,color))
    x_imgtopics.set_axis_off();

    x_time.clear()
    x_time.set_frame_on(False)
    x_time.patch.set_visible(False) # make background of the time indicator plot transparent
    x_time.set_axis_off();
    x_time.plot([t,t],[0,1],'b--',linewidth=5.0)

    #x_topic_similarity.clear()
    #x_topic_similarity.plot(timestamps, topic_similarity_for_time(current_time))

    x_topic_dist_current.clear()
    print 'topic dist from file: ', topics[t]
    x_topic_dist_current.bar(range(vocabsize),topics[t,topic_order],color=color)

    x_time.set_xlim(time_viewport)
    x_topics.set_xlim(time_viewport)
    x_ppx.set_xlim(time_viewport)
    x_topic_ppx.set_xlim(time_viewport)
    #x_topic_similarity.set_xlim(time_viewport)
    canvas.draw()

def zoom_out(event):
    global win, canvas, is_zoomed_in, fig
    canvas = FigureCanvas(fig)
    click_connetion_id=canvas.mpl_connect('button_press_event', onclick)
    key_connetion_id=canvas.mpl_connect('key_release_event', onkey)
    canvas.setFocusPolicy( QtCore.Qt.ClickFocus )
    canvas.setFocus()
    win.setCentralWidget(canvas)

def onclick(event):
    global current_time, x_topics, x_time, x_ppx, x_img, imgdata, win, canvas, is_zoomed_in
    global imgzoom_click_connetion_id, imgzoom_key_connetion_id, canvas_imgzoom
    if event.inaxes==x_topics or event.inaxes==x_ppx or event.inaxes==x_time:
        current_time=int(event.xdata)
        draw_current_time(int(event.xdata))
    elif event.inaxes==x_img:
        print "Zooming"
        fig_imgzoom = Figure(figsize=(1000,1000), dpi=10, facecolor=(1,1,1), edgecolor=(0,0,0))
        canvas_imgzoom = FigureCanvas(fig_imgzoom)
        x_imgzoom = fig_imgzoom.add_axes([0,0,1,1])
        x_imgzoom.imshow(imgdata)
        imgzoom_click_connetion_id=canvas_imgzoom.mpl_connect('button_press_event', zoom_out)
        imgzoom_key_connetion_id=canvas_imgzoom.mpl_connect('key_release_event', zoom_out)
        canvas=win.centralWidget()
        win.setCentralWidget(canvas_imgzoom)

def onkey(event):
    global current_time, max_time, time_viewport, show_words
    #print vars(event)
    if event.key == 'w':
        show_words = not show_words
        draw_current_time(current_time)

    if event.key == 'right':
        current_time = min(current_time+1, max_time)
        draw_current_time(current_time)
    if event.key == 'left':
        current_time = max(current_time-1, 0)
        draw_current_time(current_time)
    if event.key == '=':
        t_delta = time_viewport[1]-time_viewport[0]
        alpha=max(0.0,(current_time-time_viewport[0])/float(t_delta))
        t_delta*=0.5
        t_min = max(0,current_time-t_delta*alpha)
        t_max = min(current_time+t_delta*(1.0-alpha), max_time)
        time_viewport=[t_min,t_max]
        draw_current_time(current_time)
    if event.key == '-':
        t_delta = time_viewport[1]-time_viewport[0]
        alpha=max(0.0,(current_time-time_viewport[0])/float(t_delta))
        t_delta*=2.0
        t_min = max(0,current_time-t_delta*alpha)
        t_max = min(current_time+t_delta*(1.0-alpha), max_time)
        time_viewport=[t_min,t_max]
        draw_current_time(current_time)
    if event.key == 's':
        print "Saving plot for time:"+str(current_time)
        fig.savefig('topicviz_'+str(current_time)+'.png',transparent=True, bbox_inches='tight', pad_inches=0)
        print "Done"


def render_words(words,positions,ordering,imgw,imgh,scale):
    x=positions[0::2]
    y=positions[1::2]

    dx=imgw;
    x_min=0
    x_max=imgw-1

    dy=imgh;
    y_min=0
    y_max=imgh-1

    #weights = np.bincount(words)
    #ordering = np.argsort(weights)[::-1]
    print "Topic ordering:", ordering
    #print "Topic weights:", weights[ordering]
    print x_min, x_max, y_min, y_max, dy, dx, len(x), len(y), len(words)
    img=np.zeros((dy*scale,dx*scale),dtype=np.int32)
    img -= 1
    arr = np.arange(len(words))
    np.random.shuffle(arr)
    for i in arr:
        px=x[i]-x_min
        py=y[i]-y_min
        w=(ordering[words[i]] % 254)
        img[py*scale,px*scale]=w

    return img

def render_words2(words,positions,ordering,imgw,imgh,scale,color):
    x=positions[0::2]
    y=positions[1::2]

    dx=imgw;
    x_min=0
    x_max=imgw-1

    dy=imgh;
    y_min=0
    y_max=imgh-1

    #weights = np.bincount(words)
    #print 'weights' , weights
    #ordering = np.argsort(weights)[::-1]
    #print "Topic ordering:", ordering
    #print "Topic weights:", weights[ordering]
    #print x_min, x_max, y_min, y_max, dy, dx, len(x), len(y), len(words)
    img=np.zeros((math.ceil(dy*scale),math.ceil(dx*scale),4),dtype=np.float32)
    img -= 1
    arr = np.arange(len(words))
    np.random.shuffle(arr)

    ncolors=len(color)
    for i in arr:
        px=x[i]-x_min
        py=y[i]-y_min
#        w=(ordering[words[i]] % 254)
        img[py*scale,px*scale,:]=color[words[i] % ncolors]

    return img


def get_data_for_time(filename,t):
    print "Reading line",t+1," from file ", filename
    l=linecache.getline(filename, t+1)
    if(l != ''):
        row=map(int,l.split(','))
        row.pop(0)
        print 'read ',len(row), 'numbers'
        return row
    else:
        print "ERROR: no data found for time ",t
        return []



app = QtGui.QApplication(sys.argv)
initialize_filename()
load_topic_data()
load_ppx_data()
load_image_filenames()
print('topic_ordering:',topic_order)
n=len(timestamps)
max_time=n
time_viewport=[0,n]
color=plt.cm.jet(np.linspace(0,1,vocabsize))

# generate the plot
fig = Figure(figsize=(1000,1000), dpi=10, facecolor=(1,1,1), edgecolor=(0,0,0))
canvas = FigureCanvas(fig)

x_topics = fig.add_axes         ([0.05,0.35, 0.9, 0.2]) # left, bottom, width, height (range 0 to 1)
x_ppx =fig.add_axes             ([0.05,0.25, 0.9, 0.1])
x_topic_ppx=fig.add_axes        ([0.05,0.15, 0.9, 0.1])
#x_topic_similarity=fig.add_axes ([0.05,0.05, 0.9, 0.1])

x_time=fig.add_axes([0.05,0.03,0.9,0.54])

x_img=fig.add_axes([0.05,0.6,0.3,0.35])
x_imgtopics=fig.add_axes([0.35,0.6,0.3,0.35])
x_topic_dist_current=fig.add_axes([0.65, 0.6, 0.3, 0.35])

x_time.set_frame_on(True)
x_time.patch.set_visible(False) # make background of the time indicator plot transparent
x_time.set_xlim([0,n])

topic_dist_img=mpimg.imread(topic_plot_filename)
x_topics.imshow(topic_dist_img, aspect='auto')
#x_topics.stackplot(timestamps,np.transpose(topics[:,topic_order]),colors=color);
x_topics.set_xlim([0,n])
x_topics.set_axis_off()

ylim=[np.percentile(ppxdata,1), np.percentile(ppxdata,99)]
x_ppx.set_ylim(ylim)
x_ppx.set_xlim([0,n])
x_ppx.plot(timestamps,ppxdata)

x_topic_ppx.plot(timestamps,topic_ppx)

current_time=0
draw_current_time(current_time)

# current_img = mpimg.imread('imgs/seq/'+str(int(current_time))+'.jpg')
# imgplot=x_img.imshow(current_img)        
# x_img.set_axis_off()

# word_list=get_data_for_time(topic_filename,current_time)
# word_pos_list=get_data_for_time(topic_position_filename,current_time)
# img=render_words(word_list, word_pos_list, topic_order, current_img.shape[1], current_img.shape[0],0.1)
# masked_array = np.ma.masked_where(img==-1, img)
# #cmap = matplotlib.cm.jet
# topicimgcolors=['b']
# topicimgcolors.extend(color)
# cmap = matplotlib.colors.ListedColormap(color)
# cmap.set_bad('w',0.)
# imgtopicplot=x_imgtopics.imshow(masked_array,interpolation='nearest',cmap=cmap)
# x_imgtopics.set_axis_off();


click_connetion_id=canvas.mpl_connect('button_press_event', onclick)
key_connetion_id=canvas.mpl_connect('key_release_event', onkey)
canvas.setFocusPolicy( QtCore.Qt.ClickFocus )
canvas.setFocus()


#ax = fig.add_subplot(111)
#ax.plot([0,1])
# generate the canvas to display the plot
win = QtGui.QMainWindow()
# add the plot canvas to a window
win.setCentralWidget(canvas)

win.show()

sys.exit(app.exec_())
