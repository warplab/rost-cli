to convert windows subtitle files to unix ones (to make sure the newline character is the right one)

awk '{ sub("\r$", ""); print }' CITIZEN\ KANE.srt > ck.txt


Or split on word boundaries

\W  # Any non-word character

\b  # Any word boundary character
Or on non-words

\s  # Any whitespace character

@string1.split(%r{\W+}) 
