text_is_coming = false
the_text = ""

dic_file = File.open(ARGV[1], "r")

dictionary = []
dictionary = dic_file.read.delete("\n").split(",")

dic_file.close

IO.foreach(ARGV.first) do |line|
  
  if text_is_coming
    the_text += ","+line.delete("\n")
  end
  if line.include? "-->"
    text_is_coming = true
  end
  if line == "\n"
    text_is_coming = false
  end
  
end

data = the_text

p = data.split(%r{\W+})

p.each do |word_to_consider|
  if dictionary.index(word_to_consider.downcase) == nil
    dictionary << word_to_consider.downcase.delete("\n")
  end
end

dictionary.reject! {|c| c.empty? || c == "s"}

puts "Dic length"
puts dictionary.length


out_file = File.open(ARGV[1], "w")
out_file.puts dictionary.join(",")
out_file.close
