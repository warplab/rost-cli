var scaleFactor = 0.25;
var snapshots = [];  //list of canvases of the selected images
var timestamps = []; //timestamps for the selections

function capture(video, scaleFactor) {
    if(scaleFactor == null){
        scaleFactor = 1;
    }
    var w = video.videoWidth * scaleFactor;
    var h = video.videoHeight * scaleFactor;
    var canvas = document.createElement('canvas');
        canvas.width  = w;
        canvas.height = h;
    var ctx = canvas.getContext('2d');
        ctx.drawImage(video, 0, 0, w, h);
    return canvas;
} 

function renderSummary(){
    var output = document.getElementById('out');
    output.innerHTML = '';

    //display thumbnails
    var tr = document.createElement('tr')
    for(var i=0; i<snapshots.length; i++){
	var td = document.createElement('td')
	td.appendChild(snapshots[i]);
	tr.appendChild(td);
    }
    output.appendChild(tr);

    //display timestamps
    tr = document.createElement('tr')
    for(var i=0; i<snapshots.length; i++){
	   var td = document.createElement('td')
	   td.innerHTML = timestamps[i].toFixed(2);
	   tr.appendChild(td);
    }
    output.appendChild(tr);

    //remove button
    tr = document.createElement('tr')
    for(var i=0; i<snapshots.length; i++){
	var td = document.createElement('td');
	if(i>=1){
	    var button = document.createElement('button');
	    button.innerText='Remove'

	    button.onclick=function(idx){
		return function(){
		    snapshots.splice(idx,1);
		    timestamps.splice(idx,1);
		    renderSummary();
		}
	    }(i);

	    td.appendChild(button);
	}
	tr.appendChild(td);
	
    }
    output.appendChild(tr);

}

function onSelect(){
    var video  = document.getElementById('video');
    var output = document.getElementById('out');
    var canvas = capture(video, scaleFactor);

    snapshots.push(canvas);
    var ts = video.currentTime;
    timestamps.push(ts);

    canvas.addEventListener("click",function(){
	video.currentTime = ts;
    }, false);

    renderSummary()
}

function init(){
    video = document.getElementById('video');
    //current_time = 0;


    var timer = document.getElementById('timer');   //timer display element
    var select = document.getElementById('select');   //timer display element


    video.addEventListener('timeupdate',function(){
	   var current_time = video.currentTime;
	   timer.firstChild.nodeValue = current_time.toFixed(2);
    },false);

    video.addEventListener('loadeddata',function(){
	   onSelect(); //initialize the summary with the first image
    },false);


//    video.play();
//    video.pause();


}
window.onload = init;
