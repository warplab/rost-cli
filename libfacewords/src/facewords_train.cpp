/*
 * Copyright (c) 2011. Philipp Wagner <bytefish[at]gmx[dot]de>.
 * Released to public domain under terms of the BSD Simplified license.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the organization nor the names of its contributors
 *     may be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *   See <http://www.opensource.org/licenses/bsd-license>
 */

#include "opencv2/core/core.hpp"
#include "opencv2/face/facerec.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

using namespace cv;
using namespace cv::face;
using namespace std;
namespace po = boost::program_options;

int im_width = 128;
int im_height = 128;

///process program options..
///output is variables_map args
int read_program_options(int argc, char*argv[], po::variables_map& args){
  po::options_description desc("Extract visual words from a video file or camera.");
  desc.add_options()
    ("help", "help")
    ("detect-model,d",po::value<string>(),"INPUT: Cascade classifier model file for detecting face.")
    ("recog-dataset,r", po::value<string>(),"INPUT: CSV file for the face recognizer training dataset")
    ("recog-model,o", po::value<string>()->default_value("face_recog_model.yml"),"OUTPUT: face recognition model")
    ("recog-type,g",po::value<string>()->default_value("fisher"),"fisher = FisherFaces, eigen = EigenFaces, lbp = Locally Binary Pattern");


  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(desc)
	    .run(),
	    args);

  po::notify(args);

  if (args.count("help")) {
    cerr << desc << "\n";
    exit(0);
  }
  return 0;
}


static void read_training_data(const string& filename, vector<Mat>& images, vector<int>& labels, CascadeClassifier& face_detector, char separator = ',') {
    std::ifstream file(filename.c_str(), ifstream::in);
    if (!file) {
        string error_message = "No valid input file was given, please check the given filename.";
        CV_Error(CV_StsBadArg, error_message);
    }
    string line, path, classlabel;
    while (getline(file, line)) {
        stringstream liness(line);
        getline(liness, path, separator);
        getline(liness, classlabel);
        if(!path.empty() && !classlabel.empty()){
	  cv::Mat img = imread(path, 0);
	  vector< Rect_<int> > faces;
	  face_detector.detectMultiScale(img, faces);

	  if(faces.size()>0){

            Mat face = img(faces[0]);//take the first face
            Mat face_resized;
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
	    //	    imshow("orig",img);
	    //	    imshow("face",face_resized);
	    //	    cv::waitKey(100);
            images.push_back(face_resized);
            labels.push_back(atoi(classlabel.c_str()));
	  }
	}
    }
}


int main(int argc, char *argv[]) {

  po::variables_map args;
  read_program_options(argc,argv, args);


  // Get the path to your CSV:
  string fn_haar = args["detect-model"].as<string>();
  string fn_csv = args["recog-dataset"].as<string>();

  CascadeClassifier haar_cascade;
  cerr<<"Loading face model: "<<fn_haar<<endl;
  haar_cascade.load(fn_haar);

  // These vectors hold the images and corresponding labels:
  vector<Mat> images;
  vector<int> labels;
  cerr<<"Reading training data..";
  try {
    read_training_data(fn_csv, images, labels, haar_cascade);
    cerr<<"done."<<endl;
  } catch (cv::Exception& e) {
    cerr << "Error opening file \"" << fn_csv << "\". Reason: " << e.msg << endl;
    // nothing more we can do
    exit(1);
  }

  // Get the height from the first image. We'll need this
  // later in code to reshape the images to their original
  // size AND we need to reshape incoming faces to this size:

  // Create a FaceRecog-Type and train it on the given images:
  Ptr<FaceRecognizer> model;
  if(args["recog-type"].as<string>()=="fisher"){
    cerr<<"Using Fischer Faces"<<endl;
    model  = FisherFaceRecognizer::create();
  }
  else if(args["recog-type"].as<string>()=="eigen"){
    cerr<<"Using Eigen Faces"<<endl;
    model  = EigenFaceRecognizer::create();
  }
  else if(args["recog-type"].as<string>()=="lbp"){
    cerr<<"Using LBP Faces"<<endl;
    model  =  LBPHFaceRecognizer::create();
  }
  else{
    cerr<<"Invalid recog-type type."<<endl;
    return 0;
  }
  cerr<<"Training...";
  model->train(images, labels);
  cerr<<"Writing model to: "<<args["recog-model"].as<string>()<<endl;
  model->save(args["recog-model"].as<string>());
  cerr<<"Done."<<endl;
  return 0;
}
