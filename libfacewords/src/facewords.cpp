/*
 * Copyright (c) 2011. Philipp Wagner <bytefish[at]gmx[dot]de>.
 * Released to public domain under terms of the BSD Simplified license.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of the organization nor the names of its contributors
 *     may be used to endorse or promote products derived from this software
 *     without specific prior written permission.
 *
 *   See <http://www.opensource.org/licenses/bsd-license>
 */

#include "opencv2/core/core.hpp"
#include "opencv2/face.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <iostream>
#include <fstream>
#include <sstream>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

//using namespace boost::filesystem
using namespace cv;
using namespace face;
using namespace std;
namespace po = boost::program_options;

int im_width = 128;
int im_height = 128;

string find_default_model(string filename, char * argv0){
  char* data_root_c;
  data_root_c = getenv ("ROSTPATH");
  string data_root = string(ROSTPATH);
  if (data_root_c!=NULL){
    cerr<< "ROSTPATH:"<<data_root_c<<endl;
    data_root=string(data_root_c);
  }

  cerr<< "1111ROSTPATH:"<<data_root<<endl;

  boost::filesystem::path binpath = boost::filesystem::system_complete(argv0).parent_path();
  vector<boost::filesystem::path> search_locations(1,binpath);
  search_locations[0]=boost::filesystem::path(data_root+string("/facewords/"+filename));
//  search_locations[1]/=string("../data/"+filename);
  for(size_t i=0;i<search_locations.size(); ++i){
    if( boost::filesystem::exists(search_locations[i])){
      return  search_locations[i].string();
    }
  }
  //  cerr<<argv0<<endl;
  return string("./")+filename;
}


int read_program_options(int argc, char*argv[], po::variables_map& args){
  po::options_description desc("Extract visual words from a video file or camera.");

  char* data_root_c;
  data_root_c = getenv ("ROSTPATH");
  string data_root;
  if (data_root_c!=NULL){
    cerr<< "ROSTPATH:"<<data_root_c<<endl;
    data_root=string(data_root_c);
  }

  desc.add_options()
    ("help", "help")
    ("detect-model,d",po::value<string>()->default_value(data_root+"/share/facewords/default_detect_model.xml"),"INPUT: Cascade classifier model filename for detecting face.")
    ("recog-model,o", po::value<string>()->default_value(data_root+"/share/facewords/default_recog_model.yml.gz"),"INPUT: face recognition model filename")
//    ("detect-model,d",po::value<string>()->default_value(find_default_model("facewords/default_detect_model.xml",argv[0])),"INPUT: Cascade classifier model filename for detecting face.")
//    ("recog-model,o", po::value<string>()->default_value(find_default_model("facewords/default_recog_model.yml.gz",argv[0])),"INPUT: face recognition model filename")
    ("recog-type,g",po::value<string>()->default_value("fisher"),"fisher = FisherFaces, eigen = EigenFaces, lbp = Locally Binary Pattern")
    ("visualize,v","visualize")
    ("video", po::value<string>(),"Video file")
    ("camera", "Use Camera")
    ("weight",  po::value<int>()->default_value(1000),"number of words to emit for a full screen face")
    ("subsample",  po::value<int>()->default_value(1),"subsample (video file only)")
    ("scale",  po::value<double>()->default_value(1.0),"video scaling. 0<scale<=1.0")
    ("threshold", po::value<int>()->default_value(10),"How many neighbors each candidate rectangle should have to retain it.")
    ("out", po::value<string>()->default_value("words.face.csv"),"output timestammped csv word file")
    ;
  

  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(desc)
	    .run(), 
	    args);

  po::notify(args);    

  if (args.count("help")) {
    cerr << desc << "\n";
    exit(0);
  }
  return 0;
}
  

int main(int argc, char *argv[]) {

  po::variables_map args;
  read_program_options(argc,argv, args);


  Ptr<FaceRecognizer> model;
  if(args["recog-type"].as<string>()=="fisher"){
    cerr<<"Using Fischer Faces"<<endl;
    model  = FisherFaceRecognizer::create();
  }
  else if(args["recog-type"].as<string>()=="eigen"){
    cerr<<"Using Eigen Faces"<<endl;
    model  = EigenFaceRecognizer::create();
  }
  else if(args["recog-type"].as<string>()=="lbp"){
    cerr<<"Using LBP Faces"<<endl;
    model  =  LBPHFaceRecognizer::create();
  }
  else{
    cerr<<"Invalid recognizer type."<<endl;
    return 0;
  }
  cerr<<"Loading recognition model from file: "<<args["recog-model"].as<string>()<<endl;
  model->read(args["recog-model"].as<string>());


  CascadeClassifier haar_cascade;
  cerr<<"Loading face model: "<<args["detect-model"].as<string>()<<endl;
  haar_cascade.load(args["detect-model"].as<string>());


  int deviceId=0;
  bool camera=false;
  VideoCapture *cap;
  if(args.count("video")==0){
    cap = new VideoCapture(deviceId);
    camera=true;
  }
  else{
    cap = new VideoCapture(args["video"].as<string>());
    camera=false;
  }

  if(!cap->isOpened()) {
    cerr << "Capture Device cannot be opened." << endl;
    return -1;
  }
  cerr<<"Writing output to: "<<args["out"].as<string>()<<endl;
  ofstream out(args["out"].as<string>().c_str());
  double scale = args["scale"].as<double>();
  Mat frame;
  for(;;) {
    for(int ss=0;ss<args["subsample"].as<int>()-1; ++ss){
      cap->grab();
    }
    if(cap->grab()==false) break;
    cap->retrieve(frame);
    if(frame.empty()) break;
    Mat original = frame.clone();
    Mat gray, scaled;
    cv::resize(original,scaled,cv::Size(),scale,scale);
    original=scaled;
    cvtColor(original, gray, CV_BGR2GRAY);


    int time_msec = (int)cap->get(CV_CAP_PROP_POS_MSEC);
    cerr<<"Time: "<<time_msec/1000.0<<"sec.    \r";

    vector< Rect_<int> > faces;
    haar_cascade.detectMultiScale(gray, 
				  faces,
				  1.1, //scale factor
				  args["threshold"].as<int>() //minNeighbors
				  );

    for(int i = 0; i < faces.size(); i++) {

      int face_weight = faces[i].width/(float)gray.cols * faces[i].height / (float)gray.rows * (float)(args["weight"].as<int>());
      Mat face = gray(faces[i]);
      
      Mat face_resized;
      cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);
      
      int prediction; double confidence;
      model->predict(face_resized, prediction, confidence);

      out<<time_msec;
      for(size_t w=0;w<face_weight; ++w){
	out<<","<<prediction;
      }
      out<<endl;
      if(args.count("visualize")){
	rectangle(original, faces[i], CV_RGB(128, 128,0), 1);
	string box_text = format("label = %d conf=%f", prediction,confidence);
	
	int pos_x = std::max(faces[i].tl().x - 10, 0);
	int pos_y = std::max(faces[i].tl().y - 10, 0);
	putText(original, box_text, Point(pos_x, pos_y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(0,255,0), 2.0);
      }
    }

    if(args.count("visualize")) imshow("faces", original);

    // And display it:
    char key = (char) waitKey(20);
    // Exit this loop on escape:
    if(key == 27)
      break;
  }
  return 0;
}
