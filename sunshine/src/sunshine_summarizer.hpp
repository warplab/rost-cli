#include "rost/summarizer.hpp"

cv::Mat fit_image(cv::Mat img, int w, int h){
  float a = float(w) / float(h);
  float ai = float(img.cols) / float(img.rows);
  int rw, rh;
  if(ai > a){ // scale to fit the with
    rw = w;
    rh = float(rw)/ai;
  }
  else{
    rh = h;
    rw = float(rh)*ai;
  }
  cv::Mat r;
  cv::resize(img,r,cv::Size(rw,rh));
  return r;
}



template<typename R>
class SunshineSummarizer{
public:
  R* rost;
  vector<int> summary;
  map<int,cv::Mat> images; //[timestamp]->image
  double alpha;
  cv::Scalar background;
  double threshold = 0;
  int S;
  SunshineSummarizer(R* rost_, int S_):rost(rost_),S(S_), alpha(0.1){
    background=cv::Scalar(255,255,255);
  }

  tuple<float,vector<int>> compute_batch_summary(vector<int> timestamps, vector<vector<float>>& observations, int S){
    float surprise_score = 0;
    if(S >= timestamps.size()){
      return tie(surprise_score, timestamps);
    }
    Summary<>summary(S,"2min");
    vector<int> summary_timestamps;


    for(int i=0;i<timestamps.size()-1;++i){    
      summary.add(observations[i],timestamps[i],false); //add without trimming    
    }
    summary.update_threshold();
    surprise_score = get<0>(summary.surprise(observations[observations.size()-1]));
    cerr<<endl<<surprise_score<<"/"<<summary.threshold<<endl;
    if( (surprise_score > summary.threshold) || (observations.size()<=S))
      summary.add(observations[observations.size()-1], timestamps[timestamps.size()-1]);

    //    summary.trim(); //trim the summary to S elements
    summary_timestamps = vector<int>(summary.uids.begin(), summary.uids.end());
    return make_tuple(summary.cost, summary_timestamps);
  }

  void add_timestep(int t, cv::Mat img){
    summary.push_back(t);
    images[t]=cv::Mat();
    cv::resize(img,images[t],cv::Size(),0.5,0.5);
    vector<vector<float>> obs;
    
    for(auto ts: summary){
      vector<int> topics_for_time;
      for(auto const& p: rost->get_poses_by_time().at(ts)){
	vector<int> topics = rost->get_ml_topics_for_pose(p);	
	topics_for_time.insert(topics_for_time.end(), topics.begin(), topics.end());
      }
      vector<float> observation = normalize(histogram(topics_for_time,rost->get_num_topics()),alpha);
      obs.push_back(observation);
    }

    vector<int> new_sum; float cost;
    tie(cost,new_sum) = compute_batch_summary( summary, obs, S);
    summary=new_sum;
  }

  cv::Mat render(int window_w, int window_h){
    if(summary.size()==0) return cv::Mat();

    int nrows=std::ceil(std::sqrt(((float)summary.size())*3.0/4.0));
    int ncols=std::ceil((float)summary.size() / nrows);
    
    //cerr<<"Show sum: ";copy(summary.begin(), summary.end(), ostream_iterator<int>(cerr," ")); cerr<<endl;

    cv::Mat img0 = images[*(summary.begin())];
    
    int width=ncols*img0.cols;
    int width1=img0.cols;
    int height=nrows*img0.rows;
    int height1=img0.rows;
    int type=img0.type();
    cv::Mat simage(height,width,type);
    simage=background;

    for(size_t i=0;i<summary.size();i++){
      int r=i/ncols;
      int c=i-r*ncols;
      cv::Mat img=simage(cv::Range(r*height1,(r+1)*height1),cv::Range(c*width1,(c+1)*width1));
      images[summary[i]].copyTo(img);
    }
    simage = fit_image(simage,window_w, window_h);
    
    return simage;
  }


};
