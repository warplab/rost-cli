#!/usr/bin/env bash
set -e
mkdir -p build
pushd build
cmake .. -DCMAKE_VERBOSE_MAKEFILE="${2:-false}" -DCMAKE_BUILD_TYPE="${1:-Release}"
make -j$(nproc)
sudo make install
popd
