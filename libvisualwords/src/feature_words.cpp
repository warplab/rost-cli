#include <visualwords/flann_matcher.hpp>
#include <visualwords/feature_words.hpp>
#ifdef USE_SURF
#include <opencv2/xfeatures2d.hpp>
#endif

using namespace std;

using namespace std;

FeatureBOW::FeatureBOW(const string& vocabulary_filename,
         const vector<string>& feature_detector_names_, 
         const vector<int>& feature_sizes_, 
         const string& feature_descriptor_name_, 
         double img_scale_):
      BOW(feature_descriptor_name_, 0),
      feature_detector_names(feature_detector_names_),
      feature_descriptor_name(feature_descriptor_name_),
      img_scale(img_scale_)
{
  cerr<<"Initializing Feature BOW with detectors:";
  for(size_t i=0;i<feature_detector_names.size(); ++i){
    cerr<<feature_detector_names[i]<<endl;
    feature_detectors.push_back(get_feature_detector(feature_detector_names[i],feature_sizes_[i]));
  }

#ifdef USE_SURF
  if (feature_descriptor_name=="SURF") desc_extractor = cv::xfeatures2d::SURF::create(); else
#endif
  desc_extractor = cv::ORB::create();


  if(feature_descriptor_name=="SURF"){
    cerr<<"Using SURF descriptor"<<endl;
    desc_matcher = cv::DescriptorMatcher::create("FlannBased");
    //  desc_matcher = cv::Ptr<cv::FlannBasedMatcher>(new cv::FlannBasedMatcher());
    flann_matcher = cv::Ptr<FlannMatcher>(new L2FlannMatcher<float>());
  }
  else{
    cerr<<"Using ORB descriptor"<<endl;
    desc_matcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
    flann_matcher = cv::Ptr<FlannMatcher>(new BinaryFlannMatcher());
    //desc_matcher = cv::Ptr<cv::FlannBasedMatcher>(new cv::BinaryFlannBasedMatcher());
    //desc_matcher = new cv::BinaryFlannBasedMatcher();

    //desc_matcher = cv::Ptr<cv::DescriptorMatcher>(new cv::FlannBasedMatcher(new cv::flann::LshIndexParams(12,20,2)));
    //desc_matcher = new FlannBasedMatcher(new flann::LshIndexParams(20,10,2));

  }

  //      ROS_INFO("Opening vocabulary file: %s",vocabulary_filename.c_str());
  cv::FileStorage fs(vocabulary_filename, cv::FileStorage::READ);
  if(fs.isOpened()){
    fs["vocabulary"]>>vocabulary;
    fs.release();
    cerr<<"Read vocabulary: "<<vocabulary.rows<<" "<<vocabulary.cols<<endl;
    vocabulary_size=static_cast<int>(vocabulary.rows);
  }
  else{
    cerr<<"Error opening vocabulary file"<<endl;
    exit(0);
  }
  vector<cv::Mat> vocab_vector;
  for(int i=0;i<vocabulary.rows; ++i){
    vocab_vector.push_back(vocabulary.row(i));
  }
  desc_matcher->add(vocab_vector);
  flann_matcher->set_vocabulary(vocabulary);

}
  
WordObservation FeatureBOW::operator()(cv::Mat const& img, int const vocabulary_offset) const{
  std::vector<cv::KeyPoint> keypoints;
  cv::Mat descriptors;

  WordObservation z;
  z.source=name;
  //      z->seq = image_seq;
  //      z->observation_pose=pose;
  z.vocabulary_begin=vocabulary_offset;
  z.vocabulary_size=vocabulary_size;

  get_keypoints(img, feature_detector_names, feature_detectors, keypoints);
  if(!keypoints.empty()) desc_extractor->compute(img,keypoints,descriptors);
  if(!keypoints.empty()) flann_matcher->get_words(descriptors, z.words);

  z.word_pose.resize(keypoints.size()*2);//x,y
  z.word_scale.resize(keypoints.size());//x,y
  auto ci = z.word_pose.begin();
  auto si = z.word_scale.begin();
  auto zi = z.words.begin();
  for(auto const& keypoint : keypoints){
    (*zi++) += vocabulary_offset; //offset the word ids by vocabulary_begin
    *ci++ = static_cast<int>(keypoint.pt.x/img_scale);
    *ci++ = static_cast<int>(keypoint.pt.y/img_scale);
    *si++ = static_cast<int>(keypoint.size/img_scale);
  }
  //cerr<<"#feature-words: "<<z->words.size()<<endl;
  return z;
}



LabFeatureBOW::LabFeatureBOW(const string& vocabulary_filename,
                             const vector<string>& feature_detector_names_, 
                             const vector<int>& feature_sizes_, 
                             const string& feature_descriptor_name_, 
                             double img_scale_)
                             : BOW(string("Lab")+feature_descriptor_name_, 0)
{
  //cerr<<"Initialized LabFeatureBOW ...";;
  for(int i=0; i<3; i++){
    FeatureBOW * b = new FeatureBOW(vocabulary_filename, feature_detector_names_, feature_sizes_, feature_descriptor_name_, img_scale_);
    bow.push_back({b, vocabulary_size});
    vocabulary_size += b->get_vocab_size();
  }
  cerr<<"Initialized LabFeatureBOW with vocabulary of size:"<<vocabulary_size<<endl;
}


WordObservation LabFeatureBOW::operator()(cv::Mat const& img, int const vocabulary_offset) const{
  WordObservation z;
  z.vocabulary_size=vocabulary_size;
  z.vocabulary_begin= vocabulary_offset;
  vector<cv::Mat> img_channels;
  if(img.type() == CV_8UC1){
    img_channels.push_back(img);  
  }
  else if(img.type() == CV_8UC3 ){
    cv::Mat lab_image;
    cvtColor(img, lab_image, cv::COLOR_BGR2Lab);
    split(lab_image, img_channels);
  }
  else
    assert(false);

  //cerr<<"Lab Words: ";
  for(size_t i=0;i<img_channels.size(); i++){
    int const offset = vocabulary_offset + bow[i].second;
    WordObservation zi = (*(bow[i].first))(img_channels[i], offset);
    z.words.insert(z.words.end(), zi.words.begin(), zi.words.end());
    z.word_pose.insert(z.word_pose.end(), zi.word_pose.begin(), zi.word_pose.end());        
    z.word_scale.insert(z.word_scale.end(), zi.word_scale.begin(), zi.word_scale.end());        
    //cerr<<i<<":"<<zi.words.size()<<endl;
    //copy(zi.words.begin(), zi.words.end(), ostream_iterator<int>(cerr,","));
    //cerr<<endl;
  }
  //cerr<<endl;
  return z;
}
