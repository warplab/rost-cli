//#include <ros/ros.h>
//#include <ros/topic.h>
#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/flann/flann.hpp>
#include <iostream>
#include <fstream>
//#include <chrono>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <visualwords/timeutil.hpp>
#include "visualwords/bow.hpp"
#include "visualwords/image_source.hpp"
#include "visualwords/gabor_words.hpp"
#include "visualwords/color_words.hpp"
#include "visualwords/feature_words.hpp"
#include "visualwords/texton_words.hpp"

#include <stdlib.h>     /* getenv */


using namespace std;
//using namespace rost;
namespace po = boost::program_options;


double img_scale;




///process program options..
///output is variables_map args
int read_program_options(int argc, char*argv[], po::variables_map& args){

  char* data_root_c;
  data_root_c = getenv ("ROSTPATH");
  string data_root = string(ROSTPATH);
  if (data_root_c!=NULL){
    cerr<<"ROSTPATH: "<<data_root_c<<endl;
    data_root=data_root_c;
  }


  po::options_description desc("Extract visual words from a video file or camera.");
  desc.add_options()
    ("help", "help")
    ("video", po::value<string>(),"Video file")
    ("camera", po::value<int>(), "Use Camera with the given id. 0 => default source")
    ("image", po::value<string>(), "Image filename")
    ("image-list", po::value<string>(), "Text file containing list of image filenames")
    ("mask-list",po::value<string>(), "Text file containing list of mask image filenames")

    ("subsample", po::value<int>()->default_value(1),"Subsampling rate for input sequence of images and video.")
    ("fps",po::value<double>()->default_value(-1),"fps for input")
    ("numframe,N", po::value<int>()->default_value(0)," Number of input images (0=no limit)")
    ("scale", po::value<float>()->default_value(1.0),"Scale image")
    ("output-timestamps",po::value<bool>()->default_value(true),"If true, first column of the words output file has the timestamp.")

    ("images-out",po::value<string>()->default_value("./images"),"images are put in this folder")
    ("save-images","save time stamped images")

    ("logfile", po::value<string>()->default_value("visualwords.log"),"Log file")

    ///these don't work
    ("gabor", po::value<bool>()->default_value(false),"Enable Gabor words")
    ("gabor-cell-size", po::value<int>()->default_value(32),"Gabor words cell size")
    ("gabor-out", po::value<string>()->default_value("words.gabor.csv"),"Output file name with extracted words")
    ("gabor-visualize", po::value<bool>()->default_value(false),"Show visualization of the features")

    ("color", po::value<bool>()->default_value(false),"Enable Color words (hue and intensity)")
    ("color-cell-size", po::value<int>()->default_value(32),"Number of Color words along horizontal direction")
    ("color-out", po::value<string>()->default_value("words.color.csv"),"Output file name with extracted words")
    ("color-visualize", po::value<bool>()->default_value(false),"Show visualization of the features")
    ("color-no-intensity","No intensity words")
    ("color-no-hue","No words words")


    ("orb", po::value<bool>()->default_value(false),"Enable ORB words on grayscale images")
    ("lab-orb", po::value<bool>()->default_value(false),"Use ORB words on Lab channels")
    ("orb-vocabulary", po::value<string>()->default_value(data_root+"/share/visualwords/orb_vocab/default.yml"),"Vocabulary file name")
    ("orb-num-features", po::value<int>()->default_value(1000),"Number of features")
    ("orb-out", po::value<string>()->default_value("words.orb.csv"),"Output file name with extracted words")
    ("orb-visualize", po::value<bool>()->default_value(false),"Show visualization of the features")

    ("texton", po::value<bool>()->default_value(false),"Enable Texton words")
    ("texton-vocabulary", po::value<string>()->default_value(data_root+"/share/visualwords/texton.vocabulary.baraka.1000.csv"),"Vocabulary file name")
    ("texton-cell-size", po::value<int>()->default_value(64),"Number of words along horizontal direction")
    ("texton-out", po::value<string>()->default_value("words.texton.csv"),"Output file name with extracted words")
    ("texton-visualize", po::value<bool>()->default_value(false),"Show visualization of the features")


    ;

  //  po::positional_options_description pos_desc;
  //  pos_desc.add("task", -1);
  

  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(desc)
	    //	    .positional(pos_desc)
	    .run(), 
	    args);

  po::notify(args);    

  if (args.count("help")) {
    cerr << desc << "\n";
    exit(0);
  }
  return 0;
}
  

void output(ostream& out, ostream& out_pos,const WordObservation& z){
  out<< z.observation_pose[0];
  out_pos<< z.observation_pose[0];
  for(size_t i=0; i<z.words.size(); ++i){
    out<<","<<z.words[i];
    out_pos<<","<<z.word_pose[i*2]<<","<<z.word_pose[i*2+1];
  }
  out<<endl;
  out_pos<<endl;
}

struct Logger{
  ofstream olog;
  Logger(string filename):olog(filename.c_str(), std::ios::app){
  }
};
template<typename T>
Logger& operator<<(Logger& log, const T& s){
  log.olog<<s;
  cerr<<s;
  log.olog.flush();
  return log;
}

int main(int argc, char**argv){

  
  po::variables_map args;
  read_program_options(argc,argv, args);

  //cv::initModule_nonfree();


  Logger logger(args["logfile"].as<string>());

  vector<ostream*> output_streams;
  vector<ostream*> position_output_streams;

  vector<BOW *> word_extractors;



  if(args["color"].as<bool>()){
    word_extractors.push_back(new ColorBOW(args["color-cell-size"].as<int>(), args["scale"].as<float>(),!(args.count("color-no-hue")),!(args.count("color-no-intensity")) ));

    string word_filename=args["color-out"].as<string>();
    output_streams.push_back(new ofstream(word_filename.c_str()));
    string pos_filename(word_filename.begin(), word_filename.end()-3); pos_filename=pos_filename+"position.csv";
    position_output_streams.push_back(new ofstream(pos_filename.c_str()));

    cerr<<"Writing color words to: "<<args["color-out"].as<string>()<<endl;
    word_extractors.back()->enable_visualization(args["color-visualize"].as<bool>());
  }

  if(args["gabor"].as<bool>()){
    word_extractors.push_back(new GaborBOW(args["gabor-cell-size"].as<int>(), args["scale"].as<float>()));

    string word_filename=args["gabor-out"].as<string>();
    output_streams.push_back(new ofstream(word_filename.c_str()));
    string pos_filename(word_filename.begin(), word_filename.end()-3); pos_filename=pos_filename+"position.csv";
    position_output_streams.push_back(new ofstream(pos_filename.c_str()));

   cerr<<"Writing gabor words to: "<<args["gabor-out"].as<string>()<<endl;
    word_extractors.back()->enable_visualization(args["gabor-visualize"].as<bool>());
  }

  if(args["orb"].as<bool>()){
    vector<string> orb_feature_detector_names={string("ORB")};
    vector<int> orb_num_features(1,args["orb-num-features"].as<int>());
    word_extractors.push_back(new FeatureBOW(
					     args["orb-vocabulary"].as<string>(), 
					     orb_feature_detector_names,
					     orb_num_features,
					     "ORB",
					     args["scale"].as<float>()));

    string word_filename=args["orb-out"].as<string>();
    output_streams.push_back(new ofstream(word_filename.c_str()));
    string pos_filename(word_filename.begin(), word_filename.end()-3); pos_filename=pos_filename+"position.csv";
    position_output_streams.push_back(new ofstream(pos_filename.c_str()));

   cerr<<"Writing orb words to: "<<args["orb-out"].as<string>()<<endl;
    word_extractors.back()->enable_visualization(args["orb-visualize"].as<bool>());
  }

  if(args["lab-orb"].as<bool>()){
    vector<string> orb_feature_detector_names={string("ORB")};
    vector<int> orb_num_features(1,args["orb-num-features"].as<int>());
    word_extractors.push_back(new LabFeatureBOW(
               args["orb-vocabulary"].as<string>(), 
               orb_feature_detector_names,
               orb_num_features,
               "ORB",
               args["scale"].as<float>()));

    string word_filename=args["orb-out"].as<string>();
    output_streams.push_back(new ofstream(word_filename.c_str()));
    string pos_filename(word_filename.begin(), word_filename.end()-3); pos_filename=pos_filename+"position.csv";
    position_output_streams.push_back(new ofstream(pos_filename.c_str()));

    cerr<<"Writing orb words to: "<<args["orb-out"].as<string>()<<endl;
    word_extractors.back()->enable_visualization(args["orb-visualize"].as<bool>());
  }

  if(args["texton"].as<bool>()){
    word_extractors.push_back(new TextonBOW(args["texton-cell-size"].as<int>(), args["scale"].as<float>(), args["texton-vocabulary"].as<string>() ));

    string word_filename=args["texton-out"].as<string>();
    output_streams.push_back(new ofstream(word_filename.c_str()));
    string pos_filename(word_filename.begin(), word_filename.end()-3); pos_filename=pos_filename+"position.csv";
    position_output_streams.push_back(new ofstream(pos_filename.c_str()));

    cerr<<"Writing gabor words to: "<<args["texton-out"].as<string>()<<endl;
    word_extractors.back()->enable_visualization(args["texton-visualize"].as<bool>());
  }


  assert(position_output_streams.size()==output_streams.size());

  logger<<"Using word extractors: \nname -- V\n";
  for(size_t i=0;i<word_extractors.size(); ++i){
    logger<<word_extractors[i]->get_name()<<" -- "<<word_extractors[i]->get_vocab_size()<<"\n";
  }

  string images_out_prefix=args["images-out"].as<string>()+"/";
  if(args.count("save-images")){
    boost::filesystem::create_directories(args["images-out"].as<string>());
  }

  ImageSource * img_source;

  double duration=0;
  bool using_camera=false, using_video=false, using_img=false;
  if(args.count("video")){
    logger<<"Opening video file: "<<args["video"].as<string>()<<"\n";
    img_source = ImageSource::videofile(args["video"].as<string>());
    duration=dynamic_cast<CVImageSource*>(img_source)->duration_msec();
    logger<<"Video duration: "<<duration/1000.0<<"seconds\n";
    using_video=true;
    dynamic_cast<CVImageSource*>(img_source)->grab();
    //    dynamic_cast<CVImageSource*>(img_source)->reset();
  
  }
  else if(args.count("image")){
    img_source = ImageSource::imagefile(args["image"].as<string>());
    using_img=true;
  }
  else if(args.count("image-list")){
    logger<<"Opening text file with filename: "<<args["image-list"].as<string>()<<"\n";
    img_source = ImageSource::imagefilelist(args["image-list"].as<string>());
    using_img=true;
  }  
  else{
    img_source = ImageSource::camera(args["camera"].as<int>());
    using_camera=true;
  }

  ImageSource * mask_source=NULL;
  bool using_mask=false;
  if(args.count("mask-list")){
    logger<<"Opening text file with mask filename: "<<args["mask-list"].as<string>()<<"\n";
    mask_source = ImageSource::imagefilelist(args["mask-list"].as<string>());
    using_mask=true;
  }

  Subsample subsample(args["subsample"].as<int>());
  Scale scale(args["scale"].as<float>());
  //pipeline
  *img_source > subsample > scale;

  bool done=false;   int numframe=0, imgid=0;
  cv::Mat img, mask_img;
  vector<double> pose(1,0);
  long time0 = epoch_time();
  while(!done && (args["numframe"].as<int>()==0 || numframe < args["numframe"].as<int>())){

    //figure out the timestamp for this frame
    if(using_camera){
      //pose[0]=std::chrono::system_clock::now().time_since_epoch() /  std::chrono::milliseconds(1); 
      pose[0]=(epoch_time() - time0);
    }
    else if(using_video){
      pose[0]=dynamic_cast<CVImageSource*>(img_source)->pos_msec();
    }
    else{
      //pose[0]=(epoch_time() - time0);
      pose[0]=numframe;
    }

    done=!scale.grab();      
    if(done)
      {std::cerr<<"Done reading files.\n"; continue;}
    scale.retrieve(img);

    if(using_mask) {
      mask_source->grab();
      mask_source->retrieve(mask_img);
    }

    if(args.count("save-images")){
      char fname[1024];
      sprintf(fname,"%s%f.jpg",images_out_prefix.c_str(),pose[0]);
      //cv::imwrite(images_out_prefix+to_string(pose[0])+".jpg",img);
      cv::imwrite(fname,img);
    }
    
    for(size_t xi=0; xi<word_extractors.size(); ++xi){
      WordObservation z = (*word_extractors[xi])(img);
      z.seq = imgid;
      z.observation_pose = pose;
      if(using_mask) z = apply_mask(z, mask_img);

      output(*(output_streams[xi]),*(position_output_streams[xi]),z);
    }
    cerr<<pose[0]/1000.0<<" sec. ";
    if(duration!=0) cerr<<"("<<pose[0]/duration*100.0<<"%)";
    cerr<<"            \r";
    imgid+=args["subsample"].as<int>();
    numframe++;
    cv::waitKey(1); 
  }

  return 0;
}
