#include <visualwords/color_words.hpp>
using namespace std;

static constexpr int HUE_VOCAB_SIZE = 180;
static constexpr int INTENSITY_VOCAB_SIZE = 256;

ColorBOW::ColorBOW(int size_cols_, double img_scale_, bool use_hue_, bool use_intensity_):
  BOW("", 0),
  size_cols(size_cols_),
  img_scale(img_scale_),
  use_hue(use_hue_), use_intensity(use_intensity_)
{
  if(use_hue) name="Hue";
  if(use_intensity) name+="Light";

  assert(use_hue || use_intensity);
  if(use_hue){
    cerr<<"Initializing Hue Words"<<endl;
    vocabulary_size+=HUE_VOCAB_SIZE;
  }
  if(use_intensity){
    cerr<<"Initializing Intensity Words"<<endl;
    vocabulary_size+=INTENSITY_VOCAB_SIZE;
  }
}

WordObservation ColorBOW::operator()(cv::Mat const& imgs, int const vocabulary_offset) const{
  cv::Mat thumb;
  int size_rows = size_cols*static_cast<float>(imgs.rows)/imgs.cols;
  cv::resize(imgs,thumb,cv::Size(size_cols,size_rows));
  cv::Mat hls (size_rows, size_cols, CV_8UC3);
  cv::Mat_<uchar> hue (hls.rows, hls.cols);
  cv::Mat_<uchar> saturation (hls.rows, hls.cols);
  cv::Mat_<uchar> lightness (hls.rows, hls.cols);
  cv::cvtColor(thumb, hls, cv::COLOR_BGR2HLS);
  cv::Mat splitchannels[]={hue, lightness, saturation};
  cv::split(hls, splitchannels);


  WordObservation z;
  z.source=name;
  //      z.seq = image_seq;
  //      z.observation_pose=pose;
  z.vocabulary_begin= vocabulary_offset;
  z.vocabulary_size=vocabulary_size;

  //width of each pixel in the original image
  float word_scale = static_cast<float>(imgs.cols)/size_cols/img_scale;
  int const hvocab0 = vocabulary_offset;
  int const ivocab0 = vocabulary_offset + ((use_hue) ? HUE_VOCAB_SIZE : 0);

  for(int i=0;i<thumb.rows; ++i) // y
    for(int j=0;j<thumb.cols; ++j){ //x
      if(use_intensity){
        z.words.push_back(ivocab0 + lightness(i, j));
        z.word_pose.push_back(j*word_scale + word_scale/2);
        z.word_pose.push_back(i*word_scale + word_scale/2);
        z.word_scale.push_back(word_scale/2);
      }
      if(use_hue){
        z.words.push_back( hvocab0 + hue(i,j)); //hue is from 0..180
        z.word_pose.push_back(j*word_scale + word_scale/2);
        z.word_pose.push_back(i*word_scale + word_scale/2);
        z.word_scale.push_back(word_scale/2);
      }
    }
  //cerr<<"#color-words: "<<z->words.size();
  return z;
}

