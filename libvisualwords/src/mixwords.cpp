#include <visualwords/csv_reader.hpp>
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <limits>
#include <numeric>

using namespace std;
namespace po = boost::program_options;

///process program options..
///output is variables_map args
int read_program_options(int argc, char*argv[], po::variables_map& args){
  po::options_description desc("Mixes time stamped CSV word files from different sources into one big file.");
  desc.add_options()
    ("help", "help")
    ("out,o",po::value< string>()->default_value("-"), "output files")
    ("in,i",po::value< vector<string> >(), "list of word files and vocabulary size in format: \n<V1> words1.csv <V2> words2.csv <V3> words3.csv...")
    ("timestep,t", po::value<int>()->default_value(0),"time step")
    ("logfile", po::value<string>()->default_value("mixwords.log"),"Log file")
    ;

  po::positional_options_description pos_desc;
  pos_desc.add("in", -1);
  

  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(desc)
	    .positional(pos_desc)
	    .run(), 
	    args);

  po::notify(args);    

  if (args.count("help")) {
    cerr << desc << "\n";
    exit(0);
  }
  return 0;
}
struct Logger{
  ofstream olog;
  Logger(string filename):olog(filename.c_str()){
  }
};
template<typename T>
Logger& operator<<(Logger& log, const T& s){
  log.olog<<s;
  cerr<<s;
  log.olog.flush();
  return log;
}

void mix(vector<csv_reader>& csv, const vector<int>& vocabsize, ostream& out, int timestep){
  //compute wordid offsets={0,V1,V1+V2,V1+V2+V3,....}
  vector<int>offset(vocabsize.size()+1,0);
  partial_sum(vocabsize.begin(), vocabsize.end(), offset.begin()+1);

  vector<int > next_time(csv.size());
  int last_time=0;
  int n = csv.size();
  bool done=true;
  for(int i=0;i<n;++i){
    csv[i].get();
    assert(!csv[i].words.empty());
    next_time[i]=csv[i].words[0];
  }
  size_t i = min_element(next_time.begin(), next_time.end()) - next_time.begin();
  int current_time = next_time[i];
  last_time = current_time;
  out<<current_time;

  bool first=true;
  while(!csv.empty()){
    size_t i = min_element(next_time.begin(), next_time.end()) - next_time.begin();
    current_time = next_time[i];

    if(timestep > 0)
      while(current_time > last_time + timestep){
	last_time += timestep;
	out<<endl<<last_time;
      }
    else if(current_time > last_time){
      last_time = current_time;
      out<<endl<<current_time;
    }

    for(size_t j=1;j<csv[i].words.size(); ++j){
      out<<","<<csv[i].words[j]+offset[i];
    }
    csv[i].get();

    if(csv[i].words.empty()){
      csv.erase(csv.begin()+i);
      next_time.erase(next_time.begin()+i);
    }
    else{
      next_time[i] = csv[i].words[0];
    }
  }
  out<<endl;
}

int main(int argc, char**argv){
  po::variables_map args;
  read_program_options(argc,argv, args);

  vector<string> input = args["in"].as<vector<string> >();
  if(input.size() %2 !=0 ){
    cerr<<"Format of input is:  V1 word1.csv V2 word2.csv V3 word3.csv ...."<<endl;
    return 0;
  }

  Logger logger(args["logfile"].as<string>());

  vector<string> filename;
  vector<csv_reader> csv;
  vector<int> vocabsize;
  int V=0;
  for(size_t i=0;i<input.size(); i+=2){
    vocabsize.push_back(atoi(input[i].c_str()));
    filename.push_back(input[i+1]);
    csv.emplace_back(csv_reader(filename.back()));
    logger<<filename.back()<<" -- "<<vocabsize.back()<<"\n";
    V+=vocabsize.back();
  }
  logger<<"V = "<<V<<"\n";
  logger<<"timestep = "<<args["timestep"].as<int>()<<"\n";

  if(args["out"].as<string>()=="-")
    mix(csv,vocabsize,cout,args["timestep"].as<int>());
  else{
    ofstream out(args["out"].as<string>().c_str());
    mix(csv,vocabsize,out,args["timestep"].as<int>());
  }
  return 0;
}
