#include <visualwords/timeutil.hpp>
#include <chrono>
long epoch_time(){
	return std::chrono::system_clock::now().time_since_epoch() /  std::chrono::milliseconds(1);
}