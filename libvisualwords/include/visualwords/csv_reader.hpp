#ifndef ROST_CSV_READER_HPP
#define ROST_CSV_READER_HPP
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
/*
  Word I/O
*/

struct csv_reader{
  std::istream* stream;
  std::string line;
  size_t doc_size;
  char delim;
  std::vector<int>words;
  csv_reader(std::string filename, char delim_=','):
    delim(delim_)
  {
    if(filename=="-" || filename == "/dev/stdin"){
      stream  = &std::cin;
    }
    else{
      stream = new std::ifstream(filename.c_str());
    }
  }  
  /*  std::vector<int> get(){
    std::vector<int> words;
    getline(*stream,line);
    if(*stream){
      std::stringstream ss(line);
      copy(istream_iterator<int>(ss), istream_iterator<int>(), back_inserter(words));
    }
    return words;
    }*/
  std::vector<int> get(){
    words.clear();
    std::vector<std::string> words_str;
    std::string word;
    getline(*stream,line);
    if(*stream){
      //cerr<<"Read line: "<<line<<endl;
      std::stringstream ss(line);
      while(std::getline(ss,word,delim)){
        words_str.push_back(word);
      }
      transform(words_str.begin(), words_str.end(), back_inserter(words), [](const std::string& s){return atoi(s.c_str());});
    }
    return words;
  }

  std::vector<float> get_floats(){
    std::vector<float>fwords;

    std::vector<std::string> words_str;
    std::string word;
    getline(*stream,line);
    delim=','; //i don't know why delim gets reset sometimes!!!!:(((
    if(*stream){
      //cerr<<"Read line: "<<line<<endl;
      std::stringstream ss(line);
      while(std::getline(ss,word,delim)){
        words_str.push_back(word);
      }
      //cerr<<"# elements:"<<words_str.size()<<" delim:"<<(char)delim<<endl;
      transform(words_str.begin(), words_str.end(), back_inserter(fwords), [](const std::string& s){return atof(s.c_str());});
    }
    return fwords;
  }

  std::vector<int> peek(){
    return words;
  }
  ~csv_reader(){
    if(stream != &std::cin && stream !=NULL){
      //delete stream;
      stream=0;
    }
  }
};



#endif
