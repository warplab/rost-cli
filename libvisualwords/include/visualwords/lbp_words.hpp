  struct LBPBOW:public BOW{
    cv::RNG rng;
    cv::Mat_<unsigned char> gray;
    cv::Mat_<unsigned char> tmp;
    int num_words;
    LBPBOW(int vocabulary_begin_=0, double img_scale_=1.0, int num_words_=1000):
      BOW("LBP",vocabulary_begin_,256),
      num_words(num_words_)
    {
      ROS_INFO(">>>>>>>>>>>>>>>>>>>>>>>>>>>>Inilizing LBP words: %d",num_words);
    }
    WordObservation::Ptr operator()(cv::Mat& imgs, unsigned image_seq, const vector<int>& pose){
      ROS_INFO("LBP: %d",image_seq);

      WordObservation::Ptr z(new rost_common::WordObservation);
      z->source="LBP";
      z->seq = image_seq;
      z->observation_pose=pose;
      z->vocabulary_begin=vocabulary_begin;
      z->vocabulary_size=256;

      int nwords=num_words;
      gray.create(imgs.rows,imgs.cols);
      cvtColor(imgs,tmp,CV_BGR2GRAY);

      float local_scale=0.25;
      cv::resize(tmp,gray,cv::Size(),local_scale,local_scale);
      vector<int> word_hist(256);
      for(int i=0;i<nwords;++i){
	int scale = 1;
	int x = rng.next()%(gray.cols -8) +4;
	int y = rng.next()%(gray.rows -8) +4;
	int word =0;
	//ROS_INFO("LBP x:%d, y:%d  word:%d",x,y,word);
	fill(word_hist.begin(), word_hist.end(), 0);
	int r=0;
	for(int rx=x-r; rx<=x+r; ++rx)	for(int ry=y-r; ry<=y+r; ++ry){
	    word=0;
	    for(int gx=rx-1; gx <=rx+1; gx++) for(int gy=ry-1; gy <=ry+1; gy++){
	      //ROS_INFO("LBP gx:%d, gy:%d  word:%d",gx,gy,word);
	      if(gx != rx || gy !=ry)
		{
		  word = word << 1;
		  word = word | (gray.at<unsigned char>(gy,gx) > gray.at<unsigned char>(ry,rx)); 
		  //cerr<<word<<endl;
		  //assert(word<256);
		}
	      }
	    word_hist[word]++;
	  }
	vector<int>::iterator it = max_element(word_hist.begin(), word_hist.end());
	//copy(word_hist.begin(), word_hist.end(), ostream_iterator<int>(cerr," "));cerr<<endl;
	word = it - word_hist.begin();
	//cerr<<"word= "<<word<<endl;
	z->words.push_back(vocabulary_begin + word); 
	z->word_pose.push_back(x/local_scale);
	z->word_pose.push_back(y/local_scale);
	z->word_scale.push_back(scale*2/local_scale);	
      }        
      return z;
    }
  };

