#ifndef ROST_FLANN_MATCHER
#define ROST_FLANN_MATCHER
#include "flann/flann.hpp"
#include <opencv2/opencv.hpp>
#include <memory>


class FlannMatcher{
public:
  virtual void set_vocabulary(cv::Mat vocab)=0;
  virtual void get_words(cv::Mat q, std::vector<int>& words)=0;
};

class BinaryFlannMatcher: public FlannMatcher{
  typedef flann::Hamming<unsigned char> Distance;
  typedef Distance::ElementType ElementType;
  typedef Distance::ResultType DistanceType;
  flann::Matrix<unsigned char> data;
  flann::Matrix<unsigned char> query;
  flann::Matrix<size_t> match;
  flann::Matrix<DistanceType> dists;
  flann::Matrix<DistanceType> gt_dists;
  flann::Matrix<int> indices;
  std::unique_ptr<flann::Index<Distance>> index = nullptr;
  flann::IndexParams index_params = flann::HierarchicalClusteringIndexParams();
  flann::SearchParams search_params;
public:
  BinaryFlannMatcher() = default;

  void set_vocabulary(cv::Mat vocab) override {
    data = flann::Matrix<unsigned char>(vocab.data, vocab.rows, vocab.cols, vocab.step[0]);
    index = std::make_unique<flann::Index<Distance>>(data, index_params);
    index->buildIndex();
  }

  void get_words(cv::Mat q, std::vector<int>& words) override {
    assert(static_cast<int>(q.cols) == static_cast<int>(data.cols));
    if (!index) throw std::runtime_error("Cannot get words without vocabulary");
    query = flann::Matrix<unsigned char>(q.data, q.rows, q.cols, q.step[0]);
    std::vector<DistanceType> dists_vec(query.rows);
    dists = flann::Matrix<DistanceType>(&(dists_vec[0]), query.rows, 1);
    words.resize(query.rows);
    indices = flann::Matrix<int>(&(words[0]), query.rows, 1);    
    index->knnSearch(query, indices, dists, 1, flann::SearchParams());
  }
};

template<typename Float=float>
class L2FlannMatcher: public FlannMatcher{
  typedef typename flann::L2<Float> Distance;
  typedef typename Distance::ElementType ElementType;
  typedef typename Distance::ResultType DistanceType;
  flann::Matrix<ElementType> data;
  flann::Matrix<ElementType> query;
  flann::Matrix<size_t> match;
  flann::Matrix<DistanceType> dists;
  flann::Matrix<DistanceType> gt_dists;
  flann::Matrix<int> indices;
  std::unique_ptr<flann::Index<Distance>> index;
public:

  void set_vocabulary(cv::Mat vocab) override {
    data = flann::Matrix<ElementType>(reinterpret_cast<ElementType*>(vocab.data), vocab.rows, vocab.cols, vocab.step[0]);
    index = std::make_unique<flann::Index<Distance>>(data, flann::LinearIndexParams());
    index->buildIndex();
  }

  void get_words(cv::Mat q, std::vector<int>& words) override {
    if (!index) throw std::runtime_error("Cannot get words without vocabulary");
    query = flann::Matrix<ElementType>(reinterpret_cast<ElementType*>(q.data), q.rows, q.cols, q.step[0]);
    std::vector<DistanceType> dists_vec(query.rows);
    dists = flann::Matrix<DistanceType>(&(dists_vec[0]), query.rows, 1);
    words.resize(query.rows);
    indices = flann::Matrix<int>(&(words[0]), query.rows, 1);
    index->knnSearch(query, indices, dists, 1, flann::SearchParams());
  }
};

#endif
