#pragma once
#ifndef ROST_BOW_HPP
#define ROST_BOW_HPP
#include <opencv2/opencv.hpp>
#include <utility>
#include <vector>
#include <string>


//namespace rost{  

  struct WordObservation{
    unsigned int seq;
    std::string source; //name of the word source
    std::vector<int> words, //list of words in this observation
      word_scale, //size of words in this observation
      word_pose;  //pose of each word in this cell
    std::vector<double>  observation_pose; //pose of this observation

    //words are in the range [vocabulary_begin, vocabulary_begin + vocabulary_size)
    int vocabulary_begin, vocabulary_size;
    WordObservation():vocabulary_size(0),vocabulary_begin(0){}
  };

  class BOW{
  protected:
    std::string name;
    int vocabulary_size;
    bool visualize; //enable visualization

  public:
    BOW(std::string name_, int vocab_size)
    : name(std::move(name_))
    , vocabulary_size(vocab_size)
    , visualize(false){}
    virtual WordObservation operator()(cv::Mat const& img, int vocabulary_offset = 0) const =0;
    void enable_visualization(bool v){visualize=v;}
    std::string get_name() const {
        return name;
    }
    int get_vocab_size() const {
        return vocabulary_size;
    }
  };

  class MultiBOW: public BOW{
    std::vector<std::pair<BOW*, int>> bow;
    public:
    MultiBOW():BOW("Multi:",0){}
    void add(BOW * b){
        bow.push_back({b, vocabulary_size});
        name += std::string(" ") + b->get_name();
        vocabulary_size += b->get_vocab_size();
    }
    virtual WordObservation operator()(cv::Mat const& img, int const vocabulary_offset = 0) const override final {
      WordObservation z;
      z.vocabulary_size=vocabulary_size;
      z.vocabulary_begin=vocabulary_offset;
      for(auto & i : bow){
        auto const offset = vocabulary_offset + i.second;
        WordObservation const zi = (*i.first)(img, offset);
        z.words.reserve(z.words.size() + zi.words.size());
        z.words.insert(z.words.end(), zi.words.cbegin(), zi.words.cend());
        z.word_pose.reserve(z.word_pose.size() + zi.word_pose.size());
        z.word_pose.insert(z.word_pose.end(), zi.word_pose.begin(), zi.word_pose.end());
        z.word_scale.reserve(z.word_scale.size() + zi.word_scale.size());
        z.word_scale.insert(z.word_scale.end(), zi.word_scale.begin(), zi.word_scale.end());
      }
      return z;
    }
  };
//}

  /// removes the words which are not covered by the mask
  WordObservation apply_mask(const WordObservation& z, const cv::Mat& mask);
#endif
