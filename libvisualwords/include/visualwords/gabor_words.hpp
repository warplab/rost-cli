#ifndef ROST_GABOR_BOW_HPP
#define ROST_GABOR_BOW_HPP
#include "bow.hpp"

class GaborBOW:public BOW{
  int size_cols;
  double img_scale;
std::vector<double> thetas, lambdas;
std::vector<cv::Mat> sin_gabor, cos_gabor;

  public:
  GaborBOW(int size_cols_=64, double img_scale_=1.0);

  std::vector<unsigned int> make_words(std::vector<cv::Mat_<float> > const& response) const;

  virtual WordObservation operator()(cv::Mat const& imgs, int vocabulary_begin_ = 0) const override final;
};

#endif
