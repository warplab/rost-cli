#ifndef ROST_COLORWORDS
#define ROST_COLORWORDS

#include <vector>
#include <opencv2/opencv.hpp>
#include "bow.hpp"

class ColorBOW : public BOW
{
    int const size_cols;
    double const img_scale;
    bool const use_hue, use_intensity;
  public:
    explicit ColorBOW(int size_cols_ = 32, double img_scale_ = 1.0, bool use_hue_ = true, bool use_intensity_ = true);

    virtual WordObservation operator()(cv::Mat const &imgs, int vocabulary_offset = 0) const override final;
};

#endif
