#ifndef ROST_TEXTON_BOW_HPP
#define ROST_TEXTON_BOW_HPP
#include "bow.hpp"
//#include "flann_matcher.hpp"
#include<opencv2/flann/flann.hpp>

class TextonBOW:public BOW{
  int const size_cols;
  double const img_scale;
  std::vector<double> thetas, lambdas;
  std::vector<cv::Mat> sin_gabor, cos_gabor;
//std::vector<cv::Mat_<float> >filter_response;
//  cv::Ptr<FlannMatcher> flann_matcher;
//  cv::Ptr<cv::flann::GenericIndex<cv::flann::L2<float> > >flann_matcher;
  cv::flann::GenericIndex<cv::flann::L2<float> > * flann_matcher;
  cv::Mat vocabulary;

  public:
  TextonBOW(int size_cols_=64, double img_scale_=1.0, const std::string& vocab="");

  std::vector<cv::Mat> apply_filter_bank(cv::Mat const& img) const;
  std::vector<std::vector<float> > compute_feature_vector(std::vector<cv::Mat> const& response) const;
  cv::Mat compute_feature_mat(std::vector<cv::Mat> const& response) const;

  void load_vocabulary(const std::string& filename);

  virtual WordObservation operator()(cv::Mat const& img, int vocabulary_offset = 0) const final;
};

#endif
