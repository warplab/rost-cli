#include "feature_detector.hpp"
#include "opencv2/opencv.hpp"
#include "bow.hpp"

class FlannMatcher;
class FeatureBOW:public BOW{
    //    cv::Ptr<cv::FeatureDetector> feature_detector;
  std::vector<cv::Ptr<cv::FeatureDetector> >feature_detectors;
  std::vector<std::string >feature_detector_names;
  std::string feature_descriptor_name;
  cv::Ptr<cv::DescriptorExtractor> desc_extractor;
  //    cv::Ptr<cv::DescriptorMatcher> desc_matcher;
  cv::Ptr<cv::DescriptorMatcher>  desc_matcher;
  cv::Ptr<FlannMatcher> flann_matcher;
  cv::Mat vocabulary;
  
  double img_scale;

  public:
  FeatureBOW(const std::string& vocabulary_filename,
         const std::vector<std::string>& feature_detector_names_,
         const std::vector<int>& feature_sizes_,
         const std::string& feature_descriptor_name_,
	     double img_scale_=1.0);
  
  virtual WordObservation operator()(cv::Mat const& img, int vocabulary_offset = 0) const override final;
};

class LabFeatureBOW : public BOW{
    std::vector<std::pair<BOW*, int>> bow;
  public:
    LabFeatureBOW(const std::string& vocabulary_filename,
       const std::vector<std::string>& feature_detector_names_,
       const std::vector<int>& feature_sizes_,
       const std::string& feature_descriptor_name_,
       double img_scale_=1.0);

    virtual WordObservation operator()(cv::Mat const& img, int vocabulary_offset = 0) const override final;

  };
