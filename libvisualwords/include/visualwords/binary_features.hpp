#ifndef ROST_BINARY_FEATURES_HPP
#define ROST_BINARY_FEATURES_HPP
#include <opencv2/opencv.hpp>

cv::Mat binary_to_float(cv::Mat features);
cv::Mat float_to_binary(cv::Mat features);
#endif
