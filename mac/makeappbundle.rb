#!/usr/bin/ruby
require 'fileutils'
$VERBOSE = nil

binfilename = ARGV[0];
binfilenames = ['summary.kcenters','sunshine','topics.refine.path','topics.refine.t','topics.refine.txy','topics.refine.xy','words.bincount','words.extract.audio','words.extract.face','words.extract.video','words.generate.random','words.mix','words.mutualinfo','words.mutualinfo.image','words.similarity_matrix','words.vocab.audio','words.vocab.face','words.vocab.orb','words.vocab.texton','topics.render','words.plot','summary.render','words.extract.subtitles'];
binfilenames.map!{|x| '../bin/'+x}

#appdir = "./ROST.app";
appdir = "./ROST";
libdir = "#{appdir}/libs";
bindir = "#{appdir}/bin";
#bindir = "#{appdir}/Contents/MacOs";
#contentsdir= "#{appdir}/Contents";
#frameworksdir = "#{appdir}/Contents/Frameworks";
#pluginsdir = "#{appdir}/Contents/PlugIns";

#`rm -rf ROST.app`;
#`cp -r ROST.barebones.app ROST.app`
Dir.mkdir(appdir)
#Dir.mkdir(contentsdir)
Dir.mkdir(bindir)
Dir.mkdir(libdir)
#Dir.mkdir(frameworksdir)
#Dir.mkdir(pluginsdir)

ignore=[/\/usr\/lib/, /\/System\/Library/, /executable_path/]

q = binfilenames
#q.push("/opt/local/share/qt4/plugins/accessible/libqtaccessiblewidgets.dylib")
puts q;
while !(q.empty?)
  top = q.pop
  original_name=top
  puts "processing #{top}"

  if File.symlink?(top)
    top=File.readlink(top)
  end	
  basename = top.split("/")[-1]

#  basename_out = top.split("/")[-1]

  if !(File.exist?("#{bindir}/#{basename}"))
    puts "copying #{top} to #{bindir}"
    #File.copy(original_name,"#{bindir}/#{basename}");
    FileUtils.cp(original_name,"#{bindir}/#{basename}");
    `chmod u+w #{bindir}/#{basename}`;
  end
  otool_out = `otool -L #{bindir}/#{basename}`;
  lines = otool_out.split("\n")[1..-1];
  files = lines.map{|l| l.split(" ")[0]}
  deps = files.select{|f| 
    (ignore.map{|i| (i=~f)==nil}).inject(true){|bmem,b| bmem && b} 
  }
  #puts "Found #{deps.size} deps: #{deps}"
  puts "Skipping #{files - deps}"
  `install_name_tool -id @executable_path/#{basename} #{bindir}/#{basename}`
  #fix each dep reference
  deps.each{|d|
    deprealname=d;
    if File.symlink?(d)
       deprealname=File.readlink(d)
    end
    depbasename = deprealname.split("/")[-1]
    if depbasename != basename
      `install_name_tool -change #{d} @executable_path/#{depbasename} #{bindir}/#{basename}`
      q.push(d)
      puts "  adding dep: #{d}"    
    end
  }

end




