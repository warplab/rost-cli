#include <boost/program_options.hpp>
#include <iostream>

namespace po = boost::program_options;

po::options_description& load_rost_base_options( po::options_description & desc){
  desc.add_options()
    ("help", "help")    
    ("in.words,i",   po::value<std::string>()->default_value("/dev/stdin"),"Word frequency count file. Each line is a document/cell, with integer representation of words. ")
    ("in.words.delim",po::value<std::string>()->default_value(","), "delimiter used to seperate words.")
    //    ("in.cellsize",  po::value<int>()->default_value(0), "Instead of using newline to split words in cells, make each cell of this given size.")
    ("out.topics", po::value<std::string>()->default_value("topics.csv"),"Output topics file")
    ("out.topics.ml", po::value<std::string>()->default_value("topics.maxlikelihood.csv"),"Output maximum likelihood topics file")
    ("out.topicmodel", po::value<std::string>()->default_value("topicmodel.csv"),"Output topic model file")
    ("in.topicmodel", po::value<std::string>()->default_value(""),"Input topic model file")
    ("in.topics", po::value<std::string>()->default_value(""),"Initial topic labels")

    ("logfile",      po::value<std::string>()->default_value("topics.log"),"Log file")
    ("ppx.rate",     po::value<int>()->default_value(10), "Every _ iterations report perplexity.")
    ("ppx.out",     po::value<std::string>()->default_value("perplexity.csv"), "Perplexity score for each timestep")
    ("vocabsize,V",  po::value<int>(), "Vocabulary size.")
    ("ntopics,K",    po::value<int>()->default_value(100), "Topic size.")
    ("iter,n",       po::value<int>()->default_value(1000), "Number of iterations")
    ("alpha,a",      po::value<double>()->default_value(0.1), "Controls the sparsity of theta. Lower alpha means the model will prefer to characterize documents by few topics")
    ("beta,b",       po::value<double>()->default_value(1.0), "Controls the sparsity of phi. Lower beta means the model will prefer to characterize topics by few words.")
    ("online,l",     "Do online learning; i.e., output topic labels after reading each document/cell.")
    ("online.mint",  po::value<int>()->default_value(100),     "Minimum time in ms to spend between new observation timestep.")
    ("tau",          po::value<double>()->default_value(0.5), "[0,1], Ratio of local refinement (vs global refinement).")
    ("refine.weight.local", po::value<double>()->default_value(0.5), "[0,1], High value implies more importance to present time. (GeometricDistribution(X))")
    ("refine.weight.global", po::value<double>()->default_value(0.5), "[0,1], High value implies more importance to present time. (T*BetaDistribution(1/X,1))")
    ("threads", po::value<int>()->default_value(4),"Number of threads to use.")
    ("g.time", po::value<int>()->default_value(1), "Depth of the temporal neighborhood (in #cells)")
    ("g.space", po::value<int>()->default_value(1), "Depth of the spatial neighborhood (in #cells)")
    ("g.sigma", po::value<double>()->default_value(0.5), "Rate of decay of neighborhood topic distribition")
    ("cell.time",   po::value<int>()->default_value(1),"cell width in time dim ")
    ("cell.space",  po::value<int>()->default_value(32),"cell width in space dim ")    
    ;
  return desc;
}


po::variables_map read_command_line(int argc, char* argv[], po::options_description& options){

  po::variables_map args;
  po::store(po::command_line_parser(argc, argv)
	    .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
	    .options(options)
	    .run(), 
	    args);
  po::notify(args);

  if(args.count("help")){
    std::cerr<<options<<std::endl;
    exit(0);
  }
  return args;
}
