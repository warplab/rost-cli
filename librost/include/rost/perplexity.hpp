#ifndef PERPLEXITY_HPP
#define PERPLEXITY_HPP

#include <vector>
#include <algorithm>
#include <cmath>

namespace warp {

template <class PpxIt>
double document_perplexity(PpxIt begin, PpxIt end) {
    return std::accumulate(begin, end, 0, [](auto init, auto val){
        return init + log(val);
    });
}

template <class PpxContainer>
double document_perplexity(PpxContainer word_perplexities) {
    return document_perplexity(word_perplexities.begin(), word_perplexities.end());
}

}

#endif // PERPLEXITY_HPP
