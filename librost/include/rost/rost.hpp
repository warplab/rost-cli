#pragma once
#include <array>
#include <functional>
#include <random>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <mutex>
#include <thread>
#include <iterator>
#include <atomic>
#include <set>
#include <condition_variable>

#include "rost/refinery.hpp"
#include "rost/word_reader.hpp"
#include "rost/rost_types.hpp"
#include "rost/st_topic_model.hpp"

namespace warp {

    template<typename T>
    struct AtomicMatrix {
        std::vector<std::atomic<T>> flat;
        size_t const n_row;
        size_t const n_col;
        template<typename G>
        struct Row {
            std::atomic<T>* entries;
            size_t const length;

            Row(AtomicMatrix<G>* parent, size_t row)
                : entries(parent->flat.data() + parent->n_col * row)
                , length(parent->n_col)
            {
                assert(row < parent->n_row);
            }
            std::atomic<G>& operator[](size_t idx) {
                assert(idx < length);
                return entries[idx];
            }
            std::atomic<G> const& operator[](size_t idx) const {
                assert(idx < length);
                return entries[idx];
            }

            std::vector<T> copy() const {
                std::vector<T> copies;
                copies.reserve(this->length);
                for (auto i = 0; i < this->length; ++i) copies.emplace_back(this->entries[i].load());
                return copies;
            }
        };
        std::vector<Row<T>> rows;

        AtomicMatrix<T>(size_t const n_row, size_t const n_col)
                : flat(n_col * n_row), n_row(n_row), n_col(n_col) {
            if (n_row * n_col != flat.size()) throw std::logic_error("Wrong size!");
            for (auto& v : flat) v = 0;
            for (auto i = 0; i < n_row; ++i) rows.emplace_back(this, i);
        }

        AtomicMatrix<T>() = delete;
        AtomicMatrix(AtomicMatrix const& other) : flat(other.n_row * other.n_col), n_row(other.n_row), n_col(other.n_col), rows(other.rows) {
            for (size_t i = 0; i < other.flat.size(); ++i) flat[i].store(other.flat[i].load());
        }
        AtomicMatrix<T>(AtomicMatrix&& other) : flat(other.n_row * other.n_col), n_row(other.n_row), n_col(other.n_col), rows(other.rows) {
            for (size_t i = 0; i < other.flat.size(); ++i) flat[i].store(other.flat[i].load());
        }

        Row<T>& operator[](size_t const& idx) {
            return rows[idx];
        }

        Row<T> const& operator[](size_t const& idx) const {
            return rows[idx];
        }

        std::atomic<T>& operator()(size_t const& row, size_t const& col) {
            assert(row < n_row && col < n_col);
            return flat[row * n_col + col];
        }

        std::atomic<T> const& operator()(size_t const& row, size_t const& col) const {
            assert(row < n_row && col < n_col);
            return flat[row * n_col + col];
        }
    };

template<typename PoseT,
        typename PoseNeighborsT=neighbors<PoseT>,
        typename PoseHashT=hash_container<PoseT>,
        typename PoseEqualT=pose_equal<PoseT>
>
struct ROST final : public SpatioTemporalTopicModel<PoseT, PoseNeighborsT, PoseHashT, PoseEqualT> {
    typedef SpatioTemporalTopicModel<PoseT, PoseNeighborsT, PoseHashT, PoseEqualT> Super;
    typedef PoseT pose_t;
    typedef typename PoseT::value_type pose_dim_t;
    typedef std::unordered_map<PoseT, //cell_pose
            std::tuple<
                    std::vector<int>, //words
                    std::vector<int>, //topics
                    std::vector<pose_t> //word poses
            >,
            PoseHashT,
            PoseEqualT
    > word_data_t;
private:
    size_t const K; // topic size
    std::atomic<size_t> active_K;
    static constexpr size_t MAX_K = 100;
public:
    size_t const V;
    float const alpha, beta, gamma;
    float const betaV;
    std::vector<float> gamma_i;

private:
    std::uniform_int_distribution<int> uniform_K_distr;
    AtomicMatrix<int> nWZ; //nWZ[w][z] = freq of words of type w in topic z

    std::vector<std::atomic<int>> weight_Z;
//    std::vector<std::vector<int>> nZZ; //nZZ[k1][k2] = number of times we have seen topic k1 and k2 together in a cell
//    std::vector<int> nZZ_weight; //marginalized nZZ;
    std::vector<std::mutex> global_Z_mutex;
//    std::mutex global_ZZ_mutex;
//    std::atomic<size_t> activity; //number of threads actively using the topic model
public:
    std::vector<int> get_weight_Z() const {
        return {weight_Z.begin(), weight_Z.end()};
    }

    std::vector<std::vector<int>> get_nZW() const {
        std::vector<std::vector<int>> nZW(K, std::vector<int>(V, 0));
        for (size_t w = 0; w < V; w++) {
            auto const& row = nWZ[w];
            for (size_t k = 0; k < K; k++) {
                nZW[k][w] = row[k];
            }
        }
        return nZW;
    }

//    std::vector<int> mask_Z; //topic mask
    std::mutex gamma_update_mutex;
    bool update_global_model; //if true, with each iteration teh global topic model is updated along with the local cell topic model
    // if false, only the local cell model is updated
    bool fixed_k; //is true if LDA, false if HDP to grow topics
    int new_k; //id of the next new topic(only applicable when fixed_k==false)
//    bool use_topic_cooccurence; //if true then topic priors include information of cooccuring topics

    ROST(size_t V_, size_t K_, double alpha_, double beta_, const PoseNeighborsT &neighbors_ = PoseNeighborsT(),
         const PoseHashT &pose_hash_ = PoseHashT(), double gamma_ = 0.0)
            : Super(V_, neighbors_, pose_hash_),
              V(Super::V),
              K(K_),
              active_K(K_),
              beta(beta_), alpha(alpha_), gamma(gamma_), gamma_i(K_, 1.0),
              betaV(beta_ * Super::V),
              uniform_K_distr(0, K_ - 1),
              nWZ(V_, K_),
              weight_Z(K_),
//              nZZ(K_, std::vector<int>(K_, 0)),
//              nZZ_weight(K_, 0),
//              mask_Z(K_, 1),
              global_Z_mutex(K_),
              fixed_k(true),
//              use_topic_cooccurence(false),
              update_global_model(true),
              new_k(-1) {
        if (K > MAX_K) throw std::logic_error("K is too large -- increase MAX_K");
        else if (K <= 1) throw std::logic_error("K must be at least 2");
        for (auto& v : weight_Z) v = 0;
    }

    ROST(ROST const& other) = delete;
    ROST(ROST&& other) = delete;


    double perplexity(bool recompute = false) {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        //std::lock_guard<std::mutex> lock(cells_mutex);
        for (auto &cell : this->cells) {
            if (recompute)
                estimate(*cell, true);
            n_words += cell->Z.size();
            seq_ppx += cell->perplexity;
        }
        return exp(-seq_ppx / n_words);
    }

    double perplexity(const PoseT &pose, bool recompute = false) {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = this->get_cell(this->cell_lookup[pose]);
        if (recompute)
            estimate(*cell, true);
        n_words += cell->Z.size();
        seq_ppx += cell->perplexity;

        return exp(-seq_ppx / n_words);
    }

    double time_perplexity(pose_dim_t t, bool recompute = false) {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        //std::lock_guard<std::mutex> lock(cells_mutex);
        for (auto &pose : this->poses_for_time.lower_bound(t)->second) {
            auto cell = this->get_cell(this->cell_lookup[pose]);
            if (recompute)
                estimate(*cell, true);
            n_words += cell->Z.size();
            seq_ppx += cell->perplexity;
        }
        return exp(-seq_ppx / n_words);
    }

    std::vector<float> word_perplexity(const PoseT &pose, bool recompute = false) {
        auto cell = this->get_cell(this->cell_lookup[pose]);
        if (recompute)
            estimate(*cell, true);

        return cell->W_perplexity;
    }

    double topic_perplexity(const PoseT &pose) {
        double seq_ppx = 0;
        int n_words = 0;
        double const alpha_total = alpha * K;
        double W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + alpha_total;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = this->get_cell(this->cell_lookup[pose]);
        for (size_t i = 0; i < cell->nZ.size(); ++i) {
            seq_ppx -= double(cell->nZ[i]) * log(double(std::max<int>(0, weight_Z[i]) + alpha) / double(W));
            assert(seq_ppx == seq_ppx);
        }
        n_words = cell->Z.size();
        if (n_words == 0)
            return 1.0;
        else
            return exp(seq_ppx / n_words);
    }


    //compute topic perplexity given a topic distribution
    // returns the geometric mean of 1/P(topics[i] | weight_Z), where weight_Z is the current global topic distribution
    double cell_perplexity_topic(const std::vector<int> &nZ) {
        double seq_ppx = 0;
        int n_words = 0;
        double const alpha_total = alpha * K;
        double W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + alpha_total;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        for (size_t i = 0; i < K; ++i) {
            seq_ppx -= double(nZ[i]) * log(double(weight_Z[i] + alpha) / W);
        }
        n_words += accumulate(nZ.begin(), nZ.end(), 0);
        if (n_words == 0)
            return 1.0;
        else
            return exp(seq_ppx / n_words);
    }

    // returng the geometric mean of 1/P(word[i] | nWZ, nZ), where nWZ is the current topic model (VxK), and nZ is the topic distribution prior
    // nZ could be a local or global topic distribution.
    double cell_perplexity_word(const std::vector<int> &words, const std::vector<int> &nZ) const {
        int weight_c = accumulate(nZ.begin(), nZ.end(), 0);
        double const alpha_total = alpha * K;

        double ppx_sum = 0;
        for (const int &w: words) {
            double p_word = 0;
            auto const nwZ = nWZ[w];
            for (size_t k = 0; k < nZ.size(); ++k) {
                p_word += (nwZ[k] + beta) / (weight_Z[k] + beta * V) * (nZ[k] + alpha) / (weight_c + alpha_total);
            }
            ppx_sum += log(p_word);
        }
        return exp(-ppx_sum / words.size());
    }

    std::vector<float> word_topic_perplexity(const PoseT &pose) {
        int W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + K;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = this->get_cell(this->cell_lookup[pose]);
        size_t n = cell->Z.size();
        std::vector<float> result(n);
        for (size_t i = 0; i < cell->Z.size(); ++i) {
            result[i] = W / double(weight_Z[i] + 1);
        }
        return result;
    }


    [[deprecated]] void set_topic_model(const std::vector<int> &new_nZW, const std::vector<int> &new_weight_Z_dep) {
        assert(K * V == new_nZW.size());
        assert(new_weight_Z_dep.size() == K);
        std::vector<int> new_weight_Z(K, 0);
        for (int k = 0; k < K; ++k) {
            const size_t offset = k * V;
            int weight = 0;
            for (size_t w = 0; w < V; w++) {
                nWZ(w,k) = new_nZW[offset + w];
                new_weight_Z[k] += new_nZW[offset + w];
            }
#ifndef NDEBUG
            if (new_weight_Z_dep[k] != weight) throw std::invalid_argument("invalid weight_z vector!");
#endif
        }
        weight_Z = {new_weight_Z.begin(), new_weight_Z.end()};
    }

    void set_topic_model(activity_manager::WriteToken const &token,
                         const std::vector<std::vector<int>> &new_nZW,
                         [[deprecated]] const std::vector<int> &new_weight_Z = {}) {
        if (new_nZW.empty() || new_nZW.size() < K || new_nZW[0].size() != V) { // is the <K check important?
            throw std::invalid_argument("Cannot set a nZW matrix which is smaller than original matrix");
//        } else if (!new_weight_Z.empty() && new_weight_Z.size() != new_nZW.size()) {
//            throw std::invalid_argument("Invalid topic weight vector!");
        }
        if (new_nZW.size() != this->K) throw std::logic_error("Cannot set different size topic model.");
//        if (new_nZW.size() != this->K) {
//            // TODO: is there a better way to update K?
//            this->K = new_nZW.size();
//            this->uniform_K_distr = std::uniform_int_distribution<int>(0, K - 1);
//            this->gamma_i.resize(K);
//            this->mask_Z.resize(K);
//            this->global_Z_mutex = std::vector<std::mutex>(K);
//            for (size_t w = 0; w < V; ++w) {
//                nWZ[w].resize(K);
//            }
//        }
//        if (new_weight_Z.empty()) {
        for (auto& wz : weight_Z) wz = 0;
        for (int k = 0; k < K; ++k) {
            for (size_t w = 0; w < V; ++w) {
                assert(new_nZW[k][w] >= 0);
                nWZ(w,k) = new_nZW[k][w];
                weight_Z[k] += new_nZW[k][w];
            }
            assert(new_weight_Z.empty() || new_weight_Z[k] == weight_Z[k]);
        }
//        } else {
//            for (int k = 0; k < K; ++k) {
//                for (size_t w = 0; w < V; ++w) {
//                    assert(new_nZW[k][w] >= 0);
//                    nWZ(w,k) = new_nZW[k][w];
//                }
//            }
//            weight_Z = new_weight_Z;
//        }
    }

    std::vector<int> get_topic_weights() const {
        return {weight_Z.begin(), weight_Z.end()};
    }

    //returns the KxV topic-word distribution matrix
    std::vector<std::vector<int>> get_topic_model() const override final {
        return get_nZW();
    }

    //compute maximum likelihood estimate for topics in the cell for the given pose
    std::vector<int> get_ml_topics_for_pose(const PoseT &pose, bool update_ppx = false) {
        //std::lock_guard<std::mutex> lock(cells_mutex);
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            return estimate(*c, update_ppx);
        } else
            return std::vector<int>();
    }

    //compute maximum likelihood estimate for topics in the cell for the given pose
    std::tuple<std::vector<int>, double> get_ml_topics_and_ppx_for_pose(const PoseT &pose) {
        std::vector<int> topics;
        double ppx = 0;
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            topics = estimate(*c, true);
            ppx = c->perplexity;
        }
        return make_tuple(topics, ppx);
    }

    std::tuple<std::vector<int>, double> get_topics_and_ppx_for_pose(const PoseT &pose) {
        std::vector<int> topics;
        double ppx = 0;
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            topics = c->Z;
            ppx = c->perplexity;
        }
        return make_tuple(topics, ppx);
    }

//    void update_cooccurence_counts(const std::vector<std::vector<int>> &delta, int m = 1) {
//        std::lock_guard<std::mutex> lock(global_ZZ_mutex);
//        for (int i = 0; i < K; ++i) {
//            nZZ_weight[i] = 0;
//            for (int j = 0; j < K; ++j) {
//                nZZ[i][j] += m * delta[i][j];
//                nZZ_weight[i] += nZZ[i][j];
//            }
//        }
//    }

//    void update_cooccurence_counts(const std::vector<std::vector<int>> &delta_sub,
//                                   const std::vector<std::vector<int>> &delta_add) {
//        std::lock_guard<std::mutex> lock(global_ZZ_mutex);
//        for (int i = 0; i < K; ++i) {
//            nZZ_weight[i] = 0;
//            for (int j = 0; j < K; ++j) {
//                nZZ[i][j] += (delta_add[i][j] - delta_sub[i][j]);
//                nZZ_weight[i] += nZZ[i][j];
//            }
//        }
//    }

    inline void add_count(int w, int z, int c = 1) {
        // main performance cost is the locking
        if (c == 0) return;
        int old_nWZ, old_weight;
        {
//            std::lock_guard<std::mutex> lock(global_Z_mutex[z]);
            old_weight = weight_Z[z].fetch_add(c);
            old_nWZ = nWZ(w, z).fetch_add(c);
        }
        assert(old_nWZ >= std::max(0, -c));
        assert(old_weight >= std::max(0, -c));
    }

    inline void relabel(int w, int z_old, int z_new) {
        // TODO should this use a write token?
        if (z_old == z_new) return;

        add_count(w, z_old, -1);
        add_count(w, z_new, 1);
//        auto const old_nWZ = nWZ(w,z_old).fetch_sub(1);
//        auto const old_weight = weight_Z[z_old].fetch_sub(1);
//        assert(old_nWZ > 0 && old_weight > 0);
//        auto const new_nWZ = nWZ(w,z_new).fetch_add(1);
//        auto const new_weight = weight_Z[z_new].fetch_add(1);
//        assert(new_nWZ >= 0 && new_weight >= 0);
    }

    void shuffle_topics() {
        // this needs a write token
        for (auto &c : this->cells) {
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            for (size_t i = 0; i < c->Z.size(); ++i) {
                int const z_old = c->Z[i];
                int const w = c->W[i];
                int const z_new = uniform_K_distr(this->engine);
                relabel(w, z_old, z_new);
                c->relabel(w, z_new);
            }
        }
    }

    template<typename WordContainer>
    void add_observation(const PoseT &pose, const WordContainer &words) {
        add_observation(pose, words.begin(), words.end(), true, words.end(), words.end());
    }

    template<typename WordIt>
    void add_observation(const PoseT &pose, WordIt word_begin, WordIt word_end, bool update_topic_model = true) {
        add_observation(pose, word_begin, word_end, update_topic_model, word_end, word_end);
    }

    template<typename WordIt>
    void add_observation(const PoseT &pose, WordIt begin, WordIt end, bool update_topic_model, WordIt topicBegin,
                         WordIt topicEnd) {
        Super::template add_observation<WordIt>(pose, begin, end, update_topic_model, topicBegin, topicEnd);
    }

    bool forget(int cid = -1) {
        if (cid < 0)
            cid = random.uniform(0, this->C - 1);
        auto c = this->get_cell(cid);
        std::lock_guard<std::mutex> lock(c->cell_mutex);
        for (size_t i = 0; i < c->W.size(); ++i) {
            add_count(c->W[i], c->Z[i], -1);
        }
        c->forget();
        c->shrink_to_fit();
        return true;
    }


    //update the gamma_i variable.
    //gamma_i = 1.0,   for all active topics (i.e., topics with weight > 0)
    //                 topics 0,1 are always active
    //gamma_i = gamma, for the first "new" topic
    //gamma_i = 0.0 ,  otherwise
    void update_gamma() {
        std::lock_guard<std::mutex> lock(this->gamma_update_mutex);

        bool first = true;
        gamma_i[0] = 1.0;
        gamma_i[1] = 1.0;
        size_t active_K_ = 2;
        for (int k = 2; k < K; ++k) {
            if (weight_Z[k] == 0) {
                if (first) {
                    gamma_i[k] = gamma;
                    new_k = k;
                    first = false;
                    active_K_ = k + 1;
                } else gamma_i[k] = 0;
            } else {
                gamma_i[k] = 1.0;
                active_K_ = k + 1;
            }
        }
        active_K = active_K_;
    }

    //enable automatic topic size growth (Using Chineese Restaurant Process)
    void enable_auto_topics_size(bool v = true) {
        if (gamma == 0.) throw std::logic_error("Cannot enable auto topic size when gamma is 0");
        fixed_k = !v;
        if (fixed_k) {
            for (auto &g : gamma_i) {
                g = 1.0;
            }
            new_k = -1;
        } else {
            update_gamma();
        }
    }

    /// This is where all the magic happens.
    /// Refine the topic labels for Cell c, given the cells in its spatiotemporal neighborhood (nZg), and
    /// topic model nWZ. A gibbs sampler is used to randomly sample new topic label for each word in the
    /// cell, given the priors.


    //this funciton is for quickly computing topic labels wor the given list of words, without updating the model.
    std::vector<int> refine_ext(const std::vector<int> &words,        //list of input words
                                size_t n_iter = 10,                //number of refine iterations
                                std::vector<int> nZg = std::vector<int>()   //neighborhood prior topic distribution, not including the topics for the given list of words
    ) {
        return this->sampleTopics(words, n_iter, nZg);
    }

//    std::vector<int> cooccurence_diffusion(const std::vector<int> &dist) {
//        std::vector<int> out(K);
//        for (int k = 0; k < K; ++k) {
//            for (int i = 0; i < K; ++i) {
//                out[i] = int(float(nZZ[k][i]) / float(nZZ_weight[k]) * dist[k]);
//            }
//        }
//        return out;
//    }

    void refine(Cell &c, bool blocking = true) override final {
        if (c.id >= this->C) return;

        std::unique_lock<std::mutex> guard(c.cell_mutex, std::defer_lock);
        if (blocking) {
            guard.lock();
        } else if (!guard.try_lock()) {
            return;
        }

        std::array<double, MAX_K> cz{};
        std::array<float, MAX_K> prior_plus_alpha{};
        std::vector<int> nZg = this->neighborhood(
                c); //topic distribution over the neighborhood (initialize with the cell)
//        std::vector<std::vector<int>> cell_nZZ;
//        if (use_topic_cooccurence) {
//            nZg = cooccurence_diffusion(nZg);
//            cell_nZZ = c.nZZ;
//        }
        float const alpha_ = this->alpha;
        assert(alpha_ > 0);
        if (nZg.size() != K) throw std::logic_error("nZg.size() != K");
        for (size_t i = 0; i < nZg.size(); ++i) prior_plus_alpha[i] = nZg[i] + alpha_;

        auto const cz_begin = cz.begin();
        for (size_t i = 0; i < c.W.size(); ++i) {
            int const w = c.W[i];
            int const z = c.Z[i];
            if (nZg[z] <= 0) throw std::logic_error("Cannot decrement neighborhood prior any further!");
            prior_plus_alpha[z]--;
            // computeTopicCdf is the hottest part of this function
            auto const kMax = active_K.load(); // kMax <= K is always true
            this->computeTopicCdf(cz, kMax, w, prior_plus_alpha);
            auto const z_new = random.category_cdf(cz_begin, cz.begin() + kMax);
            prior_plus_alpha[z_new]++;


            if (update_global_model) relabel(w, z, z_new);
            c.relabel(i, z_new);
            if (z_new == new_k) update_gamma(); //new topic created
        }
        ++(this->refine_count);
        this->word_refine_count.fetch_add(c.W.size());

//        if (use_topic_cooccurence) {
//            c.update_cooccurence_counts();
//            update_cooccurence_counts(cell_nZZ, c.nZZ);
//        }
    }

    /// Estimate maximum likelihood topics for the cell
    /// This function is similar to the refine(), except instead of randomly sampling from
    /// the topic distribution for a word, we pick the most likely label.
    /// We do not save this label, but return it.
    /// the topic labels from this function are useful when we actually need to use the topic
    /// labels for some application.
    std::vector<int> estimate(Cell &c, bool update_ppx = false) {
        //wait_till_unpaused();
        if (c.id >= this->C)
            return std::vector<int>();

        std::vector<int> nZg = this->neighborhood(
                c); //topic distribution over the neighborhood (initialize with the cell)
//        if (use_topic_cooccurence) {
//            nZg = cooccurence_diffusion(nZg);
//        }

        int weight_c = 0;
        double ppx_sum = 0;
        if (update_ppx) {
            c.W_perplexity.resize(c.W.size());
            c.perplexity = 0;
            weight_c = accumulate(c.nZ.begin(), c.nZ.end(), 0);
        }

        std::vector<double> pz(K, 0);
        std::vector<int> Zc(c.W.size());

        double const alpha_total = alpha * K;
        for (size_t i = 0; i < c.W.size(); ++i) {
            int w = c.W[i];
            int z = c.Z[i];
            nZg[z]--; // decrement the original topic count
            if (update_ppx) ppx_sum = 0;

            auto const& nwZ = nWZ[w];
            for (size_t k = 0; k < K; ++k) {
                int nkw = nwZ[k];
                int weight_k = weight_Z[k];
                pz[k] = (nkw + beta) / (weight_k + beta * V) * (nZg[k] + alpha) * gamma_i[k];

                //      if(update_ppx) ppx_sum += pz[k]/(weight_g + alpha*K);
                if (update_ppx) {
                    ppx_sum +=
                            (nkw + beta) / (weight_k + beta * V) * (c.nZ[k] + alpha) / (weight_c + alpha_total) * gamma_i[k];
                }

            }

            nZg[z]++; // re-increment the original topic count

            if (update_ppx) {
                c.W_perplexity[i] = ppx_sum;
                c.perplexity += log(ppx_sum);
            }
            Zc[i] = max_element(pz.begin(), pz.end()) - pz.begin();
        }
        //if(update_ppx) c.perplexity=exp(-c.perplexity/c.W.size());

        return Zc;
    }

    // topic_model interface
public:
    std::vector<int> computeMaxLikelihoodTopics(const PoseT &doc) override final {
        return get_ml_topics_for_pose(doc, false);
    }

    uint32_t get_num_topics() const {
        return K;
    }

    uint32_t get_active_topics() const {
        return active_K.load();
    }

    // topic_model interface
protected:
    void inline __attribute__((hot, optimize(3))) computeTopicCdf(std::array<double, MAX_K>& cz, size_t const kMax, int const word, std::array<float, MAX_K> const &prior_plus_alpha) const {
        // Note that the prior nWZ[k] already has the old label removed
        double sum = 0.0;
        auto const& nwZ = nWZ[word];
        for (size_t k = 0; k < kMax; ++k) {
            int const nK = weight_Z[k];
            float const gk = gamma_i[k];
            assert(prior_plus_alpha[k] > 0);
            assert(nwZ[k] >= 0);
            assert(nK >= 0);
//            assert(nK == 0 || gk == 1.0); // this fails spuriously on race conditions
            if (nK != 0)      sum += (nwZ[k] + beta) * static_cast<double>(prior_plus_alpha[k]) / (nK + betaV); // implies gk == 1.0
            else if (gk != 0) sum += beta * static_cast<double>(prior_plus_alpha[k]) * gk / betaV; // implies nK == 0 and nwZ == 0
            cz[k] = sum;
        }
    }

    std::vector<float> __attribute__((hot, optimize(3))) computeTopicCdf(int const word, const std::vector<int> &prior) override final {
        // Note that the prior nWZ[k] already has the old label removed
        std::array<double, MAX_K> cz;
        std::array<float, MAX_K> alpha_plus_prior;
        auto const alpha_ = alpha;
        for (auto i = 0; i < K; ++i) alpha_plus_prior[i] = prior[i] + alpha_;
        this->computeTopicCdf(cz, K, word, alpha_plus_prior);
        return {cz.begin(), cz.begin() + K};
    }

    void addWordObservation(int word, int newTopic, bool update_topic_model) override final {
        if (update_topic_model && update_global_model)
            add_count(word, newTopic);
        if (newTopic == new_k)
            update_gamma();
    }

    int computeRandomTopic() const override final {
        return random.category_pdf(gamma_i.begin(), gamma_i.end());
    }
};
}
