//
// Created by stewart on 2/5/20.
//

#ifndef ROST_ACTIVITY_MANAGER_HPP
#define ROST_ACTIVITY_MANAGER_HPP

#include <atomic>
#include <mutex>
#include <memory>
#include <chrono>
#include <cassert>
#include <utility>

class activity_manager : public std::enable_shared_from_this<activity_manager> {
    mutable std::mutex token_mutex;
    mutable std::condition_variable pause_condition;
    std::atomic<size_t> readers = {0}, writers = {0};

    enum class Mode {
        READ, HALT, WRITE, SHUTDOWN
    };
    std::atomic<Mode> mode = {Mode::READ};

    activity_manager() = default;

    void register_token(std::unique_lock<std::mutex> const &lock, std::atomic<size_t> &counter) {
        assert(lock.mutex() == &token_mutex && lock.owns_lock());
        counter++;
    }

    void unregister_token(std::atomic<size_t> &counter) {
        assert(counter > 0);
        counter--;
        pause_condition.notify_all();
    }

    void set_mode(std::unique_lock<std::mutex> const &lock, Mode new_mode) {
        assert(lock.mutex() == &token_mutex && lock.owns_lock());
        mode = new_mode;
    }

    template<typename Condition, typename Duration = std::chrono::milliseconds>
    void wait_for_condition(std::unique_lock<std::mutex> &lock, Condition const &condition,
                            Duration timeout = Duration(-1)) const {
        assert(lock.mutex() == &token_mutex && lock.owns_lock());
        if (timeout.count() > 0) {
            pause_condition.wait_for(lock, timeout, condition);
        } else if (timeout.count() < 0) {
            while (!condition()) pause_condition.wait_for(lock, std::chrono::milliseconds(10));
        }
    }

public:
    class ReadOnlyToken;

    class ReadWriteToken;

    class ReadToken {
        ReadToken() = default;

        friend class activity_manager::ReadOnlyToken;

        friend class activity_manager::ReadWriteToken;

    public:
        virtual ~ReadToken() = default;
    };

    class WriteToken {
        WriteToken() = default;

        friend class activity_manager::ReadWriteToken;

    public:
        virtual ~WriteToken() = default;
    };

    class ReadOnlyToken final : public ReadToken {
        friend class activity_manager;

        std::weak_ptr<activity_manager> _mgr;
        decltype(std::chrono::steady_clock::now()) const creation_time;

        explicit ReadOnlyToken(std::shared_ptr<activity_manager> const &mgr,
                               std::unique_lock<std::mutex> const &pause_lock)
                : _mgr(mgr), creation_time(std::chrono::steady_clock::now()) {
            assert(mgr->mode == Mode::READ);
            mgr->register_token(pause_lock, mgr->readers);
        }

    public:
        ~ReadOnlyToken() final {
            // Note that mode may have changed while reading
            if (auto mgr = _mgr.lock()) {
                mgr->unregister_token(mgr->readers);
            }
#ifndef NDEBUG
            static size_t num_created = 0;
            static std::chrono::nanoseconds average_lifespan{0};
            auto const lifetime = std::chrono::steady_clock::now() - creation_time;
            average_lifespan = (average_lifespan * num_created + lifetime) / (num_created + 1);
            num_created += 1;
//            if (lifetime > std::chrono::milliseconds(100)) {
//                std::cerr << "Read token held for over 100 ms!" << std::endl;
//            }
#endif
        }
    };

    class ReadWriteToken final : public ReadToken, public WriteToken {
        friend class activity_manager;

        std::weak_ptr<activity_manager> _mgr;
        decltype(std::chrono::steady_clock::now()) const creation_time;

        explicit ReadWriteToken(std::shared_ptr<activity_manager> const &mgr,
                                std::unique_lock<std::mutex> const &pause_lock)
                : _mgr(mgr), creation_time(std::chrono::steady_clock::now()) {
            assert(mgr->mode == Mode::WRITE);
            mgr->register_token(pause_lock, mgr->writers);
        }

    public:
        ~ReadWriteToken() final {
            if (auto mgr = _mgr.lock()) {
                assert(mgr->mode != Mode::READ); // no readers should have started while this was alive
                mgr->unregister_token(mgr->writers);
            }
#ifndef NDEBUG
            static size_t num_created = 0;
            static std::chrono::nanoseconds average_lifespan{0};
            auto const lifetime = std::chrono::steady_clock::now() - creation_time;
            average_lifespan = (average_lifespan * num_created + lifetime) / (num_created + 1);
            num_created += 1;
//            if (lifetime > std::chrono::milliseconds(100)) {
//                std::cerr << "Write token held for over 100 ms!" << std::endl;
//            }
#endif
        }
    };

    static std::shared_ptr<activity_manager> create() {
        return std::shared_ptr<activity_manager>(new activity_manager());
    }

    template<typename Duration = std::chrono::milliseconds>
    std::unique_ptr<ReadToken> wait_for_read_token(Duration timeout = Duration(-1)) {
        std::unique_lock<std::mutex> lock(token_mutex);

        // We can't give out the token until the writer is finished
        auto const condition = [this]() {
            return mode == Mode::SHUTDOWN || mode == Mode::READ || (mode == Mode::WRITE && writers == 0);
        };
        if (!condition()) {
            // We need to switch to READ mode, but first we need to wait for writers to finish
            wait_for_condition(lock, condition, timeout);
        }

        if (mode != Mode::SHUTDOWN && condition()) {
            set_mode(lock, Mode::READ);
            return std::unique_ptr<ReadOnlyToken>(new ReadOnlyToken(this->shared_from_this(), lock));
        } else return {}; // We failed to switch into READ mode
    }

    template<typename Duration = std::chrono::milliseconds>
    std::unique_ptr<ReadWriteToken> wait_for_write_token(Duration timeout = Duration(-1)) {
        std::unique_lock<std::mutex> lock(token_mutex);

        // We can't give out the token until all readers and writers are finished
        auto const condition = [this]() { return mode == Mode::SHUTDOWN || (readers == 0 && writers == 0); };
        if (!condition()) {
            set_mode(lock, Mode::HALT); // This mode prevents new readers from starting
            wait_for_condition(lock, condition, timeout);
        }

        if (mode != Mode::SHUTDOWN && condition()) {
            set_mode(lock, Mode::WRITE);
            return std::unique_ptr<ReadWriteToken>(new ReadWriteToken(this->shared_from_this(), lock));
        } else return {}; // We failed to switch into WRITE mode
    }

    ~activity_manager() {
        // Terminate all pending token requests
        {
            std::unique_lock<std::mutex> lock(token_mutex);
            set_mode(lock, Mode::SHUTDOWN);
        }
        pause_condition.notify_all();
    }
};

#endif //ROST_ACTIVITY_MANAGER_HPP
