#ifndef ROST_DISTANCEFUN_HPP
#define ROST_DISTANCEFUN_HPP
#include <vector>
#include <iostream>
template<typename T>
class DistanceFunction{
public:
  virtual T operator()(const std::vector<T>& p, const std::vector<T>& q) const =0;
};


template <typename T>
class KLDistance:public DistanceFunction<T>{
public:
  T operator()(const std::vector<T>& p, const std::vector<T>& q) const{
    //    if(p.size() != q.size()){
    //      std::cerr<<"p.size()="<<p.size()<<"  q.size()="<<q.size()<<endl;
    //      assert(p.size() == q.size());
    //    }
    //    size_t len = std::min(p.size(), q.size());
    //T p_total = std::accumulate(p.begin(), p.begin()+len,0);
    //    T q_total = std::accumulate(q.begin(), q.begin()+len,0);
    T pv, qv;
    T d(0);
    typename std::vector<T>::const_iterator pi=p.begin(), p_end=p.end(), qi=q.begin(), q_end=q.end();
    //    for(size_t i=0;i<len; ++i){
      //      d+= ((p[i]==0 && q[i]==0) ? 0: (p[i]/p_total)*log((p[i]/p_total)/(q[i]/q_total)));
      
    //      d+= ((p[i]==0 && q[i]==0) ? 0: (p[i]*log(p[i]/q[i])));
    //    }

    for(;pi!=p_end && qi!=q_end; ++pi, ++qi){
      pv = *pi; qv=*qi;
      d+= ((pv==0 && qv==0) ? 0: (pv * log(pv/qv )));
    }
    for(;pi!=p_end; ++pi){
      pv = *pi; qv=0;
      d+= ((pv==0 && qv==0) ? 0: (pv * log(pv/qv )));
    }
    for(;qi!=q_end; ++qi){
      pv = 0; qv=*qi;
      d+= ((pv==0 && qv==0) ? 0: (pv * log(pv/qv )));
    }

    if(d<0){
      std::cerr<<"WARNING: dKL="<<d<<std::endl;
      //assert(false);
      d=0;
    }
    /*    if(d==std::numeric_limits<T>::infinity()){
      std::cerr<<"d=inf"<<endl;
      copy(p.begin(),p.end(),ostream_iterator<T>(std::cerr," ")); std::cerr<<endl;
      copy(q.begin(),q.end(),ostream_iterator<T>(std::cerr," ")); std::cerr<<endl;
      for(size_t i=0;i<p.size(); ++i){
        std::cerr<<p[i]<<"*log("<<p[i]<<"/"<<q[i]<<")="<<p[i]*log(p[i]/q[i])<<"  "   ;
      }
      std::cerr<<endl;
      }*/
    return d;
  }
};

template <typename T>
class CosDistance:public DistanceFunction<T>{
public:
  T operator()(const std::vector<T>& p, const std::vector<T>& q) const{
    //    if(p.size() != q.size()){
    T d(0);
    size_t n=std::min(p.size(), q.size());
    for(size_t i=0;i<n;++i){
      d+=p[i]*q[i];
    }
    return 1.0-d;
  }
};
#endif
