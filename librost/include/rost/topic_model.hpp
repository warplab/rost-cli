#pragma once

#include "rost/markov.hpp"
#include "rost/activity_manager.hpp"
#include <cstdint>
#include <iterator>
#include <memory>
#include <vector>

namespace warp {

thread_local fast_random<> random;

/// Most generic interface for a TopicModel of arbitrary Document, Word, and Topic types
    template<typename Document, typename DocumentId, typename Word = int, typename Topic = int,
            class WordContainer = std::vector<Word>,
            class TopicContainer = std::vector<Topic>>
    class topic_model {
    private:
        std::shared_ptr<activity_manager> activityManager = activity_manager::create();
    protected:
        uint32_t const V;
        std::mt19937 engine;

        virtual std::vector<float> computeTopicCdf(Word word, const std::vector<int> &prior) = 0;

        virtual Topic computeRandomTopic() const = 0;

        virtual void addWordObservation(Word word, Topic newTopic, bool update_topic_model) = 0;

        inline void resampleTopics(WordContainer const &words, TopicContainer &topics, TopicContainer &prior) {
            for (size_t i = 0; i < words.size(); ++i) {
                prior[topics[i]]--;
                auto const cz = this->computeTopicCdf(words[i], prior);
                auto const z_new = random.category_cdf(cz.begin(), cz.end());
                prior[z_new]++;
                topics[i] = z_new;
            }
        }

    public:
        explicit topic_model(uint32_t const vocab_size)
                : V(vocab_size) {}

        std::mt19937 &get_engine() {
            return engine;
        }

        virtual std::shared_ptr<Document> getDocument(DocumentId const &doc) const = 0;

        virtual uint32_t get_num_topics() const = 0;

        [[deprecated]] uint32_t getNumTopics() const {
            return get_num_topics();
        }

        virtual uint32_t get_num_words() const {
            return V;
        }

        virtual void addObservations(DocumentId const &doc, WordContainer const &observation_words,
                                     TopicContainer const &observation_topics = {},
                                     bool update_model = true) // TODO write lock should be part of this signature
        = 0;

        virtual void refine(Document &doc, bool blocking = true) = 0;

//    virtual void updateDocumentPerplexity(DocumentId const& doc) = 0;
        virtual TopicContainer computeMaxLikelihoodTopics(DocumentId const &doc) = 0; // TOFIX: Should be const
//    virtual std::vector<Perplexity> computeWordPerplexities(DocumentId const& doc, TopicContainer const& topic_weight_prior) const = 0;
        virtual std::vector<std::vector<int>> get_topic_model() const = 0;

        virtual std::vector<int> get_topic_weights() const = 0;

        TopicContainer
        sampleTopics(WordContainer const &words, uint32_t const sample_iterations = 10, TopicContainer prior = {}) {
            if (prior.empty()) {
                prior.resize(this->get_num_topics(), 0);
            }

            std::vector<int> topics(words.size());
            for (size_t i = 0; i < words.size(); ++i) {
                topics[i] = this->computeRandomTopic();
                prior[topics[i]]++;
            }

            for (size_t iter = 0; iter < sample_iterations; ++iter) {
                resampleTopics(words, topics, prior);
            }
            return topics;
        }

        std::unique_ptr<activity_manager::ReadToken>
        get_read_token(std::chrono::milliseconds timeout = std::chrono::milliseconds(-1)) const {
            return this->activityManager->wait_for_read_token(timeout);
        }

        std::unique_ptr<activity_manager::ReadWriteToken>
        get_write_token(std::chrono::milliseconds timeout = std::chrono::milliseconds(-1)) const {
            return this->activityManager->wait_for_write_token(timeout);
        }

        virtual void set_topic_model(activity_manager::WriteToken const &write_token,
                                     const std::vector<std::vector<int>> &phi,
                                     [[deprecated]] const std::vector<int> &topic_weights = {}) = 0;
    };
}
