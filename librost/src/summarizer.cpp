#include "rost/summarizer.hpp"
#include "rost/markov.hpp"
#include "rost/csv_reader.hpp"
#include "rost/io.hpp"

//#include "random.hpp"
#include <map>
#include <iostream>
#include <vector>
#include <limits>
using namespace std;

#include <boost/program_options.hpp>
namespace po = boost::program_options;

po::options_description& load_rost_base_options( po::options_description & desc){
  desc.add_options()
    ("help", "help")    
    ("in.words,i",   po::value<string>()->default_value("/dev/stdin"),"Word frequency count file. Each line is a document/cell, with integer representation of words. ")
    ("in.words.delim",po::value<string>()->default_value(","), "delimiter used to seperate words.")

    ("out.sum.online-kcenters.iter", po::value<string>()->default_value("summary.online-kcenters.iter.csv"),"Output online summarization iteration") 
    ("out.sum.online-kcenters", po::value<string>()->default_value("summary.online-kcenters.csv"),"Output final summary") 
    ("out.sum.kcenters", po::value<string>()->default_value("summary.kcenters.csv"),"Output final summary") 
    ("out.sum.kcenters-pp", po::value<string>()->default_value("summary.kcenters-pp.csv"),"Output final summary") 


    ("logfile",      po::value<string>()->default_value("topics.log"),"Log file") 
    //    ("vocabsize,V",  po::value<int>(), "Vocabulary size.")
    ("summarysize,S",  po::value<int>(), "Summary size.")
    ("online-kcenters",  "Compute summary in online streaming mode.")
    ("kcenters",  "Compute summary in batch mode.")
    ("kcenters-pp",  "Compute summary in batch mode.")
    ("thresholding" , po::value<string>()->default_value("auto"),"online thresholding strategy: 2min, min, doubling, mean, median")
    ("alpha", po::value<double>()->default_value(1.0),"histogram smoothing (needed for computing KL divergence)")
    ("threads", po::value<int>()->default_value(4),"Number of threads to use.")
    ;
  return desc;
}

po::variables_map read_command_line(int argc, char* argv[], po::options_description& options){

  po::variables_map args;
  po::store(po::command_line_parser(argc, argv)
      .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
      .options(options)
      .run(), 
      args);
  po::notify(args);

  if(args.count("help")){
    cerr<<options<<endl;
    exit(0);
  }
  return args;
}




template<typename S>  
tuple<float,float,float,int,vector<int>> process_online(S& summary, int timestamp, vector<float> observation){
  float surprise;
  int closest_seq;
  tie(surprise,closest_seq)=summary.surprise(observation);

  if(surprise >= summary.threshold){
    //cerr<<"Summarizer Adding : "<<msg->seq<<endl;
    summary.add(observation, timestamp);
    summary.update_threshold();
  }

  vector<int> summary_timestamps(summary.uids.begin(), summary.uids.end());
  return make_tuple(surprise, summary.threshold, summary.cost, closest_seq, summary_timestamps);
}


tuple<float,vector<int>> compute_batch_summary(vector<int> timestamps, vector<vector<float>> observations, int S){

  Summary<>summary(S);
  vector<int> summary_timestamps;

  for(int i=0;i<timestamps.size();++i){    
    summary.add(observations[i],timestamps[i],false); //add without trimming    
  }
  summary.trim(); //trim the summary to S elements
  summary_timestamps = vector<int>(summary.uids.begin(), summary.uids.end());
  return make_tuple(summary.cost, summary_timestamps);
}


tuple<float,vector<int>> compute_batch_summary_pp(vector<int> timestamps, vector<vector<float>> observations, int S){

  Summary<>summary(S);
  vector<int> summary_timestamps;

  Summary<>summary2(2);
  for(int i=0;i<timestamps.size();++i){    
    summary2.add(observations[i],timestamps[i],false); //add without trimming    
  }
  assert(summary2.uids.size()==timestamps.size());

  summary2.trim(); //trim the summary to S elements
  int uid = *(summary2.uids.begin());

  cerr<<"kcenters-pp: summary seed="<<uid<<endl;

  for(int i=0;i<timestamps.size();++i){    
    summary.add(observations[i],timestamps[i],false); //add without trimming    
  }
  summary.trim(uid); //trim the summary to S elements
  summary_timestamps = vector<int>(summary.uids.begin(), summary.uids.end());
  return make_tuple(summary.cost, summary_timestamps);
}


int main(int argc, char**argv){
  po::options_description options("Summarizer");
  load_rost_base_options(options);  
  auto args = read_command_line(argc, argv, options);


  if(args.count("summarysize")==0){
    cerr<<"Must specify summary size"<<endl;
    return 0;
  }
  /*  if(args.count("vocabsize")==0){
    cerr<<"Must specify vocabulary size"<<endl;
    return 0;
    }*/

  Summary<>summary(args["summarysize"].as<int>(),args["thresholding"].as<string>());
  csv_reader in(args["in.words"].as<string>(), args["in.words.delim"].as<string>()[0]); 
  //  int V = args["vocabsize"].as<int>();
  double alpha=args["alpha"].as<double>();


  float surprise, threshold, cost;
  int closest_timestamp;
  vector<int> summary_timestamps, timestamps;
  vector<vector<float>> observations;


  ofstream out_sum_online_iter;
  if (args.count("online-kcenters")){
    cerr<<"Writing online summary iterations to: "<<args["out.sum.online-kcenters.iter"].as<string>()<<"\n";
    out_sum_online_iter.open(args["out.sum.online-kcenters.iter"].as<string>());
  }

  vector<int> tokens = in.get();
  cerr<<"Read "<<tokens.size()<<" tokens"<<endl;
  out_sum_online_iter<<"#timestamp,surprise,threshold,cost,closest_timestamp,summary_1,....,summary_S"<<endl;
  while(!tokens.empty()){
    vector<int> words(tokens.begin()+1, tokens.end());
    int timestamp = tokens[0];
    //vector<int> words_hist = histogram(words, V);
    vector<float> observation = normalize(words,alpha); 

    //cerr<<"time: "<<timestamp<<"        \n";
    if(args.count("kcenters") || args.count("kcenters-pp")){
      timestamps.push_back(timestamp);
      observations.push_back(observation);
    }

    if(args.count("online-kcenters")){
      tie(surprise,threshold,cost,closest_timestamp,summary_timestamps) = process_online(summary, timestamp, observation);
      //iteration output
      out_sum_online_iter<<timestamp<<","<<surprise<<","<<threshold<<","<<cost<<","<<closest_timestamp;
      for(int s: summary_timestamps)
          out_sum_online_iter<<","<<s;
      out_sum_online_iter<<endl;
    }

    tokens = in.get();    
  }
  out_sum_online_iter.close();

  if(args.count("online-kcenters")){
     //dump the final output
    cerr<<"Writing final online k-centers summary to: "<<args["out.sum.online-kcenters"].as<string>()<<"\n";
    ofstream out_sum_online(args["out.sum.online-kcenters"].as<string>());
    out_sum_online  //<<"#cost"<<endl<<summary.cost<<endl
                    //<<"#summary"<<endl
      <<to_string(summary_timestamps,'\n')<<endl;
  }

  if(args.count("kcenters")){
    cerr<<"Computing batch kcenters summary\n";
    vector<int> summary_batch_timestamps;
    float cost;
    tie(cost,summary_batch_timestamps)=compute_batch_summary(timestamps,observations,args["summarysize"].as<int>());
       //dump the final output
    cerr<<"Writing batch kcenters summary to: "<<args["out.sum.kcenters"].as<string>()<<"\n";
    ofstream out_sum_batch(args["out.sum.kcenters"].as<string>());
    out_sum_batch  //<<"#cost"<<endl<<cost<<endl
		   //<<"#summary"<<endl
      <<to_string(summary_batch_timestamps,'\n')<<endl;
  }

  if(args.count("kcenters-pp")){
    cerr<<"Computing batch kcenters-pp summary\n";
    vector<int> summary_batch_timestamps;
    float cost;
    tie(cost,summary_batch_timestamps)=compute_batch_summary_pp(timestamps,observations,args["summarysize"].as<int>());
       //dump the final output
    cerr<<"Writing batch kcenters-pp summary to: "<<args["out.sum.kcenters-pp"].as<string>()<<"\n";
    ofstream out_sum_batch(args["out.sum.kcenters-pp"].as<string>());
    out_sum_batch  //<<"#cost"<<endl<<cost<<endl
		   //<<"#summary"<<endl
      <<to_string(summary_batch_timestamps,'\n')<<endl;
  }
 
  return 0;
}



