//
// Created by stewart on 2020-10-17.
//

#include <rost/markov.hpp>
#include <iostream>
//#include "testu01/bbattery.h" // TODO integrate support for running testu01 battery

int main(int argc, char** argv) {
    fast_random<> generator;
    while (true) {
        auto const next = static_cast<uint32_t>(generator.uniform_01() * UINT32_MAX);
        std::cout.write(reinterpret_cast<char const*>(&next), sizeof(next));
    }
    return 0;
}